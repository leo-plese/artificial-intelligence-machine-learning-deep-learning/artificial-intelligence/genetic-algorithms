Genetic/Evolutionary Algorithms.

Topics:

- K-tournament elimination selection (e.g. K = 3)

- Floating point (FP) and binary (BIN) solution representation

- Crossover: for FP - arithmetic and heuristic crossover, for BIN - single-point and uniform crossover

- Mutation: for FP - uniform mutation, for BIN - uniform mutation

- GA algorithm parameters: solution representation type, population size, explicit domain constraints (bottom and top limit), number of objective function evaluations

Implemented in Python.

My lab assignment in Computer Aided Analysis and Design, FER, Zagreb.

Task description in "TaskSpecification.pdf".

Created: 2020
