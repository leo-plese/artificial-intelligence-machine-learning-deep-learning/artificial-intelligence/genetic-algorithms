Programsko okruženje: PyCharm 2019.2.4
Programski jezik: Python 3.8

Upute za pokretanje:
U programskom okruženju se pokrene glavni program (bez argumenata) smješten u datoteci "main.py" sa Shift+F10 ili na tipku "Run 'main'" s označenom opcijom da se ispis na ekran snima i u datoteku results.txt koja je u korijenskom direktoriju projekta.
Ulazni parametri za genetske algoritme u svakom zadatku određeni su u datotekama ("zad{i}_params.txt", i = 1,2,3,4,5).
Pokretanjem se izvršavaju svi zadaci iz vježbe.
Rezultat svakog zadatka se ispisuje na ekran i zapisuje u datoteku results.txt (koja se generira pokretanjem programa).
Ako se želi dobiti detaljan ispis provedbe algoritama koji se vrši kod promjene najbolje jedinke i uključuje ispis broja evaluacija, trenutne najbolje jedinke i vrijednosti funkcije cilja najbolje jedinke,
treba postaviti globalnu varijablu print_yn u skripti "main.py" na True: "print_yn = True" (pretpostavljeno na False radi urednosti ispisa u svrhu usporedbe algoritama).

Napomena:
Kako se implementirani algoritmi izvršavaju duže vrijeme tj. ne bi se stigli izvršiti u roku usmenog ispitivanja vježbe, za potrebu demonstracije vježbe već su priređeni grafički prikazi - boxplots.
Boxplot grafovi nalaze se u mapi boxplots s mapama za pojedini zadatak (podmape "box{i}", i = 1,2,3,4,5). Grafovi su dobiveni nad 10 ponavljanja algoritma svake potrebne funkcije koja se testira u pojedinom zadatku.