from random import uniform, sample, randint
from math import log10, floor, ceil, sqrt, sin
from textwrap import wrap
from bitarray import bitarray
from operator import itemgetter

GA_REPEAT_TIMES = 30

print_yn = False#True

class GoalFunction():
    def __init__(self, f=None, is_multi_input=False):
        self.f = f
        self.called_times = 0
        self.is_multi_input = is_multi_input

    def evaluate(self, x, count_eval=True):
        if count_eval:
            self.called_times += 1

        if self.is_multi_input:
            return self.f(*x)
        else:
            return self.f(x)

    def reset_called_times(self):
        self.called_times = 0



def FP_create_population(pop_size, explicit_constraint, n_var):
    dg = explicit_constraint[0]
    gg = explicit_constraint[1]
    population = [[uniform(dg, gg) for _ in range(n_var)] for _ in range(pop_size)]
    return population

def evaluate_population(f_goal, population):
    population_fitness = [f_goal.evaluate(p) for p in population]
    return  population_fitness

def FP_arithmetic_crossover(parents_12, n_var):
    par1, par2 = parents_12
    a = uniform(0, 1)
    child = [a*par1[d] + (1-a)*par2[d] for d in range(n_var)]
    return child

def FP_all_explicit_constraints_ok(child, explicit_constraint):
    dg = explicit_constraint[0]
    gg = explicit_constraint[1]

    for v in child:
        if not(v >= dg and v <= gg):
            return False

    return True

def FP_heuristic_crossover(explicit_constraint, parent12_fits, parent12_sols_fits, n_var):
    better_par_ind = parent12_fits.index(min(parent12_fits))
    par2 = parent12_sols_fits.pop(better_par_ind)[0]   # better parent
    par1 = parent12_sols_fits.pop()[0]

    while True:
        a = uniform(0, 1)
        # print()
        # print("a (heur) =", a)
        child = [a * (par2[d] - par1[d]) + par2[d] for d in range(n_var)]
        # print("child (heur) =", child)
        if FP_all_explicit_constraints_ok(child, explicit_constraint):
            return child

def FP_uniform_mutation(child, explicit_constraint, pm, n_var):
    a = uniform(0, 1)
    if a <= pm:
        # print("YES mut")
        dg = explicit_constraint[0]
        gg = explicit_constraint[1]
        child = [uniform(dg, gg) for _ in range(n_var)]

    return child

def FP_runKTournamentEliminationSelection(max_f_eval, K, f_goal, pop_size, explicit_constraint, n_var, pm):
    # print("********** FP ***************")

    population = FP_create_population(pop_size=pop_size, explicit_constraint=explicit_constraint, n_var=n_var)
    population_fitness = evaluate_population(f_goal=f_goal, population=population)

    pop_sol_fit_list = list(zip(population, population_fitness))

    pop_sols = [sol[0] for sol in pop_sol_fit_list]

    if print_yn:
        fits = [sol_fit[1] for sol_fit in pop_sol_fit_list]
        best_sol_fit = pop_sol_fit_list[fits.index(min(fits))]
        best_sol_fit_so_far = best_sol_fit
        print(f_goal.called_times, ":", best_sol_fit_so_far)


    while (f_goal.called_times < max_f_eval):

        K_solutions = sample(pop_sol_fit_list, K)   # list of 3 selected (solution, fitness) pairs
        K_fits = [sol_fit[1] for sol_fit in K_solutions]
        worst_sol_ind = K_fits.index(max(K_fits))
        worst_sol_fit = K_solutions[worst_sol_ind]
        worst_sol = worst_sol_fit[0]
        pop_sol_fit_list.remove(worst_sol_fit)

        K_solutions.pop(worst_sol_ind)  # now three_solutions has 2 solutions for further selection
        K_fits.pop(worst_sol_ind)

        co_op_choice = randint(1,2)
        if co_op_choice==1:
            # 1st crossover operator
            if K==3:
                parents_12 = [sol[0] for sol in K_solutions]
            else:
                best_sol_ind_1 = K_fits.index(min(K_fits))
                best_sol_1 = K_solutions[best_sol_ind_1][0]

                K_fits[best_sol_ind_1] = float('inf')
                best_sol_ind_2 = K_fits.index(min(K_fits))
                best_sol_2 = K_solutions[best_sol_ind_2][0]

                parents_12 = [best_sol_1, best_sol_2]
            child = FP_arithmetic_crossover(parents_12, n_var)
        else:
            # 2nd crossover operator
            child = FP_heuristic_crossover(explicit_constraint, K_fits, K_solutions, n_var)

        # mutation operator
        child = FP_uniform_mutation(child, explicit_constraint, pm, n_var)

        child_fit = f_goal.evaluate(child)
        pop_sol_fit_list.append((child, child_fit))

        ## for printing - best solution so far
        if print_yn:
            fits = [sol_fit[1] for sol_fit in pop_sol_fit_list]
            best_sol_fit = pop_sol_fit_list[fits.index(min(fits))]
            if best_sol_fit[1] < best_sol_fit_so_far[1]:
                best_sol_fit_so_far = best_sol_fit
                print(f_goal.called_times, ":", best_sol_fit_so_far)

    if print_yn:
        best_sol_fit = best_sol_fit_so_far
    else:
        fits = [sol_fit[1] for sol_fit in pop_sol_fit_list]
        best_sol_fit = pop_sol_fit_list[fits.index(min(fits))]
    # best_sol = best_sol_fit[0]
    # print("BEST sol_fit =", best_sol_fit)
    # print("BEST sol =", best_sol)

    return best_sol_fit



def FP_run3TournamentEliminationSelection(max_f_eval, f_goal, pop_size, explicit_constraint, n_var, pm):
    return FP_runKTournamentEliminationSelection(max_f_eval, 3, f_goal, pop_size, explicit_constraint, n_var, pm)

##########################################################
def get_needed_number_of_bits(explicit_constraint, n_var, num_decimal):
    dg = explicit_constraint[0]
    gg = explicit_constraint[1]
    n = int(ceil(log10(floor(1+(gg-dg)*(10**num_decimal))) / log10(2)))
    return n

def convert_bin_to_real_solution(bin_sol, n_var, explicit_constraint, num_bit):
    dg = explicit_constraint[0]
    gg = explicit_constraint[1]
    real_sol = [dg + bin_sol[i] / (2 ** num_bit - 1) * (gg - dg) for i in range(n_var)]

    return real_sol


def B_create_population(pop_size, num_bit, n_var, explicit_constraint):
    max_bin_num = 2**num_bit - 1
    population_bin = [[randint(0, max_bin_num) for _ in range(n_var)] for _ in range(pop_size)]
    population_bin_formatted = [''.join([bin(b[i])[2:].zfill(num_bit) for i in range(n_var)]) for b in population_bin]

    population_real = [convert_bin_to_real_solution(b, n_var, explicit_constraint, num_bit) for b in population_bin]

    return population_real, population_bin, population_bin_formatted

def B_single_point_crossover(parents_12, n_var, num_bit):
    par1, par2 = parents_12

    co_cut_ind = randint(0, num_bit*n_var)

    child = par1[:co_cut_ind]+par2[co_cut_ind:]

    return child

def B_uniform_crossover(parents_12, n_var, num_bit):
    par1, par2 = parents_12

    random_sol = ''.join([str(randint(0,1)) for _ in range(n_var*num_bit)])

    par1_arr, par2_arr, random_sol_arr = bitarray(par1), bitarray(par2), bitarray(random_sol)
    sol_arr = (par1_arr & par2_arr) | (random_sol_arr & (par1_arr ^ par2_arr))
    child = sol_arr.to01()

    return child

def B_uniform_mutation(child, pm, n_var, num_bit):
    a_arr = [uniform(0, 1) for _ in range(n_var*num_bit)]
    inv_bit_yn = [a <= pm for a in a_arr]
    child_arr = bitarray(child)
    child = bitarray([not child_arr[i] if inv_bit_yn[i] else child_arr[i] for i in range(n_var * num_bit)]).to01()

    return child

def get_var_values(sol, n_var, explicit_constraint, num_bit):
    sol_parts = wrap(sol, num_bit)
    bin_sol = [int(part, 2) for part in sol_parts]
    real_sol = convert_bin_to_real_solution(bin_sol, n_var, explicit_constraint, num_bit)

    return real_sol



def B_runKTournamentEliminationSelection(max_f_eval, K, f_goal, pop_size, explicit_constraint, n_var, pm, num_decimal):
    # print("********** B ***************")
    num_bit = get_needed_number_of_bits(explicit_constraint, n_var, num_decimal)

    population_real, population_bin, population_bin_formatted = B_create_population(pop_size=pop_size, num_bit=num_bit, n_var=n_var, explicit_constraint=explicit_constraint)

    population_fitness = evaluate_population(f_goal=f_goal, population=population_real)

    pop_sol_fit_list = list(zip(population_bin_formatted, population_fitness))

    pop_sols = [sol[0] for sol in pop_sol_fit_list]

    if print_yn:
        fits = [sol_fit[1] for sol_fit in pop_sol_fit_list]
        best_sol_fit = pop_sol_fit_list[fits.index(min(fits))]
        best_sol_fit_so_far = best_sol_fit
        print(f_goal.called_times, ":", best_sol_fit_so_far)

    while (f_goal.called_times < max_f_eval):
        K_solutions = sample(pop_sol_fit_list, K)   # list of 3 selected (solution, fitness) pairs
        K_fits = [sol_fit[1] for sol_fit in K_solutions]
        worst_sol_ind = K_fits.index(max(K_fits))
        worst_sol_fit = K_solutions[worst_sol_ind]
        worst_sol = worst_sol_fit[0]
        pop_sol_fit_list.remove(worst_sol_fit)

        K_solutions.pop(worst_sol_ind)  # now three_solutions has 2 solutions for further selection
        K_fits.pop(worst_sol_ind)

        if K == 3:
            parents_12 = [sol[0] for sol in K_solutions]
        else:
            best_sol_ind_1 = K_fits.index(min(K_fits))
            best_sol_1 = K_solutions[best_sol_ind_1][0]

            K_fits[best_sol_ind_1] = float('inf')
            best_sol_ind_2 = K_fits.index(min(K_fits))
            best_sol_2 = K_solutions[best_sol_ind_2][0]

            parents_12 = [best_sol_1, best_sol_2]


        co_op_choice = randint(1, 2)
        if co_op_choice==1:
            # 1st crossover operator
            child = B_single_point_crossover(parents_12, n_var, num_bit)
        else:
            # 2nd crossover operator
            child = B_uniform_crossover(parents_12, n_var, num_bit)

        # mutation operator
        child = B_uniform_mutation(child, pm, n_var, num_bit)

        child_real = get_var_values(child, n_var, explicit_constraint, num_bit)

        child_fit = f_goal.evaluate(child_real)
        pop_sol_fit_list.append((child, child_fit)) # child - bin

        if print_yn:
            ## for printing - best solution so far
            fits = [sol_fit[1] for sol_fit in pop_sol_fit_list]
            best_sol_fit = pop_sol_fit_list[fits.index(min(fits))]
            if best_sol_fit[1] < best_sol_fit_so_far[1]:
                best_sol_fit_so_far = best_sol_fit
                print(f_goal.called_times, ":", best_sol_fit_so_far)


    if print_yn:
        best_sol_fit = best_sol_fit_so_far
    else:
        fits = [sol_fit[1] for sol_fit in pop_sol_fit_list]
        best_sol_fit = pop_sol_fit_list[fits.index(min(fits))]
    best_sol = best_sol_fit[0]
    # print("BEST sol_fit =", best_sol_fit)
    # print("BEST sol =", best_sol)

    best_sol_real = get_var_values(best_sol, n_var, explicit_constraint, num_bit)
    # print("BEST sol real =", best_sol_real)

    return best_sol_real, best_sol_fit[1]

def B_run3TournamentEliminationSelection(max_f_eval, f_goal, pop_size, explicit_constraint, n_var, pm, num_decimal):
    return B_runKTournamentEliminationSelection(max_f_eval, 3, f_goal, pop_size, explicit_constraint, n_var, pm, num_decimal)




def run3TournamentEliminationSelection(sol_form, max_f_eval, f_goal, pop_size, explicit_constraint, n_var, pm, num_decimal=None):
    if sol_form=="FP":
        return FP_run3TournamentEliminationSelection(max_f_eval, f_goal, pop_size, explicit_constraint, n_var, pm)
    else:   # sol_form=="B"
        return B_run3TournamentEliminationSelection(max_f_eval, f_goal, pop_size, explicit_constraint, n_var, pm, num_decimal)

def runKTournamentEliminationSelection(sol_form, max_f_eval, K, f_goal, pop_size, explicit_constraint, n_var, pm, num_decimal=None):
    if sol_form=="FP":
        return FP_runKTournamentEliminationSelection(max_f_eval, K, f_goal, pop_size, explicit_constraint, n_var, pm)
    else:   # sol_form=="B"
        return B_runKTournamentEliminationSelection(max_f_eval, K, f_goal, pop_size, explicit_constraint, n_var, pm, num_decimal)


####################

f1 = lambda x1,x2:  100 * (x2 - x1 ** 2) ** 2 + (1 - x1) ** 2
f1_goal = GoalFunction(f1, is_multi_input=True)

f3 = lambda x: sum([(x[i]-(i+1))**2 for i in range(len(x))])
f3_goal = GoalFunction(f3)

def f6(x):
    sum_xi_squares = sum([xi**2 for xi in x])
    return 0.5 + ((sin(sqrt(sum_xi_squares))) ** 2 - 0.5) / (1 + 0.001 * sum_xi_squares) ** 2
f6_goal = GoalFunction(f6)

def f7(x):
    sum_xi_squares = sum([xi**2 for xi in x])
    return (sum_xi_squares**0.25) * (1 + (sin(50 * (sum_xi_squares**0.1)))**2)
f7_goal = GoalFunction(f7)

######################

def get_hit_rate_and_median(fp_arit_list):
    fp_arit_list_fits = [sol[1] for sol in fp_arit_list]
    hit_rate =sum([f < 1e-6 for f in fp_arit_list_fits])
    median = (fp_arit_list_fits[GA_REPEAT_TIMES // 2] + fp_arit_list_fits[(GA_REPEAT_TIMES // 2) - 1]) / 2 if (GA_REPEAT_TIMES % 2 == 0) else fp_arit_list_fits[GA_REPEAT_TIMES // 2]

    return hit_rate, median


def zad1_load_params():
    fp_solve_yn, b_solve_yn = False, False

    with open("zad1_params.txt", "r") as params_file:
        lines = params_file.readlines()
        lines = [l.split("=") for l in lines]
        for l in lines:
            if l[0].strip() == "MAX_NUM_EVAL":
                MAX_NUM_EVAL = int(float(l[1].strip()))
            elif l[0].strip() == "F_LIST":
                f_num_list = l[1].strip().split()
            elif l[0].strip() == "N_VAR_LIST":
                N_VAR_LIST = l[1].strip().split()
                N_VAR_LIST = [int(el) for el in N_VAR_LIST]
            elif l[0].strip() == "FP":
                fp_solve_yn = int(l[1]) == 1
            elif l[0].strip() == "B":
                b_solve_yn = int(l[1]) == 1
            elif l[0].strip() == "POP_SIZE":
                POP_SIZE = int(l[1].strip())
            elif l[0].strip() == "EXPLICIT_CONSTRAINT":
                EXPLICIT_CONSTRAINT = l[1].strip().split()
                EXPLICIT_CONSTRAINT = tuple([float(el) for el in EXPLICIT_CONSTRAINT])
            elif l[0].strip() == "P_MUTATION_FP":
                P_MUTATION_FP = float(l[1].strip())
            elif l[0].strip() == "P_MUTATION_B":
                P_MUTATION_B = float(l[1].strip())
            else: # N_DECIMAL
                N_DECIMAL = int(l[1].strip())

        return MAX_NUM_EVAL, f_num_list, N_VAR_LIST, fp_solve_yn, b_solve_yn, POP_SIZE, EXPLICIT_CONSTRAINT, P_MUTATION_FP, P_MUTATION_B, N_DECIMAL

def zad1():
    print()
    print()
    print("************************* ZAD 1 *************************")

    MAX_NUM_EVAL, f_num_list, N_VAR_LIST, fp_solve_yn, b_solve_yn, POP_SIZE, EXPLICIT_CONSTRAINT, P_MUTATION_FP, P_MUTATION_B, N_DECIMAL = zad1_load_params()

    f_goal_list = []
    if "f1" in f_num_list:
        f_goal_list.append(f1_goal)
    if "f3" in f_num_list:
        f_goal_list.append(f3_goal)
    if "f6" in f_num_list:
        f_goal_list.append(f6_goal)
    if "f7" in f_num_list:
        f_goal_list.append(f7_goal)

    ga_type_list = []
    if fp_solve_yn:
        ga_type_list.append("FP")
    if b_solve_yn:
        ga_type_list.append("B")

    f_goal_dict = dict(zip(f_goal_list, f_num_list))


    f_sol_list = []

    f_ind = 0
    for fx_goal in f_goal_list:
        print()
        print("***** "+f_goal_dict[fx_goal]+" *****")
        fx_list = []

        n_var = N_VAR_LIST[f_ind]
        f_ind += 1

        if fp_solve_yn:
            print("FP...")
            fp_list = []
            for i in range(GA_REPEAT_TIMES):
                if print_yn:
                    print()
                    print("%5d run:" % (i+1))
                sol = run3TournamentEliminationSelection(sol_form="FP", max_f_eval=MAX_NUM_EVAL, f_goal=fx_goal, pop_size=POP_SIZE,
                                                         explicit_constraint=EXPLICIT_CONSTRAINT, n_var=n_var,
                                                         pm=P_MUTATION_FP)
                print(i, "SOL =", sol)
                fx_goal.reset_called_times()
                fp_list.append(sol)

            fp_list.sort(key=itemgetter(1))
            hit_rate, median = get_hit_rate_and_median(fp_list)
            print(":: hit rate =", hit_rate)
            print(":: median =", median)
            fx_list.append((hit_rate, median))
            for sol in fp_list:
                print(">>",sol)
            print()

        if b_solve_yn:
            print("B...")
            b_list = []
            for i in range(GA_REPEAT_TIMES):
                if print_yn:
                    print()
                    print("%5d run:" % (i + 1))
                sol = run3TournamentEliminationSelection(sol_form="B", max_f_eval=MAX_NUM_EVAL, f_goal=fx_goal, pop_size=POP_SIZE,
                                                         explicit_constraint=EXPLICIT_CONSTRAINT, n_var=n_var,
                                                         pm=P_MUTATION_B, num_decimal=N_DECIMAL)
                print(i, "SOL =", sol)
                fx_goal.reset_called_times()
                b_list.append(sol)
            b_list.sort(key=itemgetter(1))
            hit_rate, median = get_hit_rate_and_median(b_list)
            print(":: hit rate =", hit_rate)
            print(":: median =", median)
            fx_list.append((hit_rate, median))
            for sol in b_list:
                print(">>",sol)
            print()

        f_sol_list.append(fx_list)

    print("")
    print("HIT RATE / MEDIAN - comparisons")
    for i in range(len(f_num_list)):
        print("***** " + f_num_list[i] + " *****")
        fx_vals = f_sol_list[i]
        for j in range(len(ga_type_list)):
            print("  "+ga_type_list[j]+":")
            print(fx_vals[j])

    # ** ** *f1 ** ** *
    # FP...
    # :: hit rate = 10
    # :: median = 0.0
    # >> ([1.0, 1.0], 0.0)
    # >> ([1.0, 1.0], 0.0)
    # >> ([1.0, 1.0], 0.0)
    # >> ([1.0, 1.0], 0.0)
    # >> ([1.0, 1.0], 0.0)
    # >> ([1.0, 1.0], 0.0)
    # >> ([1.0, 1.0], 0.0)
    # >> ([1.0, 1.0], 0.0)
    # >> ([1.0, 1.0], 0.0)
    # >> ([1.0, 1.0], 0.0)
    #
    # B...
    # :: hit rate = 0
    # :: median = 6.709702757475879e-06
    # >> ([1.0011270940639676, 1.002265542813106], 1.2805104287079071e-06)
    # >> ([1.0010674894174159, 1.0021940172372439], 1.4747615566155098e-06)
    # >> ([1.0009125173363813, 1.0017410219234506], 1.552562710584943e-06)
    # >> ([0.9981826245243113, 0.9964421688450003], 3.844799045739628e-06)
    # >> ([1.0021105707320714, 1.004244417078624], 4.4899322169255546e-06)
    # >> ([0.9977534710691387, 0.9957090316924138], 8.929473298026204e-06)
    # >> ([0.9952500759139653, 0.9908929762510326], 3.627122139793009e-05)
    # >> ([0.9765401773613718, 0.9536519930855007], 0.0005504085411580324)
    # >> ([-4.470348493157417e-06, 0.00013262033857586175], 1.0000106995318607)
    # >> ([-4.470348493157417e-06, 0.0002160668437483082], 1.0000136092042036)
    #
    # ** ** *f3 ** ** *
    # FP...
    # :: hit rate = 8
    # :: median = 1.2685883837875243e-07
    # >> (
    # [1.000000695305603, 1.9999986462149948, 3.0000000306790153, 4.00000022105728, 4.999998783786227], 3.845167187297788e-12)
    # >> ([1.0000020683406627, 2.000002381025116, 2.9999995092468588, 4.000001866236149, 5.0000008444441235],
    #     1.4384075587239046e-11)
    # >> ([0.9999906570859006, 1.9999872410149815, 3.0000392705501167, 3.999972074310437, 4.999986242532113],
    #     2.761369909257723e-09)
    # >> ([0.9999616275375226, 2.0000072299283054, 3.0000023046824813, 3.999952527618851, 4.999982571605724],
    #     4.087405200243269e-09)
    # >> ([0.9999746705873361, 1.9997264795921725, 2.9999779709454595, 4.000144849566344, 5.00011172864017],
    #     1.0940495779224435e-07)
    # >> ([0.999930151604843, 1.9996879632576512, 3.000046789310247, 3.9998501182453814, 4.999868040869347],
    #     1.443127189652605e-07)
    # >> ([0.9996649656169223, 2.0002114312231156, 3.000065085238471, 4.000137058991019, 5.000173904237081],
    #     2.1021513891306687e-07)
    # >> (
    # [1.000006209507198, 2.000280795970867, 3.000142465439429, 3.9995807419752607, 5.000284081697768], 3.556610389817424e-07)
    # >> ([0.9993672207181941, 1.9974017169592904, 3.0043915682071285, 4.003830019225007, 4.9989514923999625],
    #     4.2205771148244626e-05)
    # >> ([1.0005763131118826, 1.9952666282314464, 2.996507243961756, 3.9916302140991253, 4.997686191772583],
    #     0.00011034331438469927)
    #
    # B...
    # :: hit rate = 0
    # :: median = 0.10620053578081098
    # >> (
    # [0.9712651661415421, 1.9395128470514038, 2.919919875857822, 3.901518997595275, 5.102522823289711], 0.031106649761259354)
    # >> (
    # [1.0021522939846577, 2.085466745062668, 3.104110154632032, 3.846640999515081, 5.154962991325938], 0.06568063288892514)
    # >> (
    # [1.0441616488743293, 1.8347934435246387, 3.164108191851021, 4.0304319271573945, 5.093868228610397], 0.06591230269932451)
    # >> (
    # [1.0813072646053783, 2.0605877655919684, 3.058500679090635, 4.230101532641093, 4.974134414617247], 0.06731982190207442)
    # >> (
    # [1.1790290826269683, 2.0507231965876542, 2.7570263372965513, 4.078211011833275, 4.921247211731881], 0.1059794198978408)
    # >> (
    # [1.3001814872080502, 2.090241077251463, 3.023578316675966, 3.9163367127280395, 4.975225179649144], 0.10642165166378119)
    # >> (
    # [0.8658841510380526, 2.12835228825665, 3.2052234770424164, 4.107250395633287, 4.834474767281847], 0.11547929640277649)
    # >> (
    # [1.1260703541657406, 1.8128112498763542, 2.8788999581009094, 4.09900707301518, 4.6837584580110985], 0.17540969590684083)
    # >> (
    # [-0.01672357370625832, 2.112586859243713, 2.950246720023351, 4.054857911314301, 4.768301688680097], 1.105571512975086)
    # >> ([-0.01182407175970468, 2.206762200795474, 3.0107335153440644, 3.8002268612452355, 5.270244338221673],
    #     1.1795950775306177)
    #
    # ** ** *f6 ** ** *
    # FP...
    # :: hit rate = 6
    # :: median = 0.0
    # >> ([7.578171045827885e-10, -5.292614385172589e-10], 0.0)
    # >> ([-1.0407429643362419e-09, -1.786178285632949e-09], 0.0)
    # >> ([1.9744107853599266e-09, -2.048699834938905e-09], 0.0)
    # >> ([9.176112863303565e-10, -1.7458212271150537e-09], 0.0)
    # >> ([-5.0052880534473794e-09, 8.150239908914661e-10], 0.0)
    # >> ([-1.214256211268968e-09, 2.314466741078333e-09], 0.0)
    # >> ([0.6623162172283475, -3.067804459605477], 0.009715909877514362)
    # >> ([-0.3076842810329228, -3.1233663500314037], 0.009715909877514362)
    # >> ([-3.0960198817654336, -0.5145365389043113], 0.009715909877514362)
    # >> ([-2.7790326366837665, -1.4584459453001954], 0.009715909877514362)
    #
    # B...
    # :: hit rate = 6
    # :: median = 9.32186378088673e-09
    # >> ([1.4901161620173298e-06, -4.470348493157417e-06], 2.2226664952995634e-11)
    # >> ([7.450580817192076e-06, 1.4901161620173298e-06], 5.778932887778865e-11)
    # >> ([-1.0430813148332163e-05, -4.470348493157417e-06], 1.2891465672737468e-10)
    # >> ([-1.639127780350691e-05, -2.8312207113856402e-05], 1.0713253062455408e-09)
    # >> ([2.5331974782716316e-05, 3.725290409306581e-05], 2.0315172877261034e-09)
    # >> ([9.685755064481327e-05, 8.493662133446378e-05], 1.6612210274047357e-08)
    # >> ([3.0720368943225367, -0.6423935485599515], 0.009715909878674767)
    # >> ([1.4056697906753328, 2.806098842802605], 0.009715909880695817)
    # >> ([-3.1028972000747075, -0.47130437109781553], 0.00971590988102411)
    # >> ([1.0544076876165747, -2.9560611532944776], 0.009715909882301477)
    #
    # ** ** *f7 ** ** *
    # FP...
    # :: hit rate = 10
    # :: median = 4.048201256997297e-46
    # >> ([-2.9436403616402465e-94, 2.2083832900997493e-94], 1.9183175702823073e-47)
    # >> ([2.752778462686552e-93, -4.045373804520927e-93], 6.995099702471163e-47)
    # >> ([6.604236262828174e-93, 2.501187195082201e-93], 8.403571933440438e-47)
    # >> ([5.312284177105862e-93, 1.770127131195187e-92], 1.3594563862111485e-46)
    # >> ([1.4636864906548748e-91, 2.7584141709739806e-92], 3.859341638823199e-46)
    # >> ([-4.278283570401134e-93, 1.7947586371879897e-91], 4.237060875171395e-46)
    # >> ([-3.303605168888469e-91, -2.4850581921457383e-91], 6.429559267245812e-46)
    # >> ([1.151315793950548e-90, -7.937332471067567e-91], 1.1825423558521498e-45)
    # >> ([-1.3865450103450014e-90, 4.32331312268194e-90], 2.1307777988955916e-45)
    # >> ([-5.293548043441164e-89, 1.507600183006284e-88], 1.264054751945148e-44)
    #
    # B...
    # :: hit rate = 0
    # :: median = 0.006297077518538908
    # >> ([1.4901161620173298e-06, -4.470348493157417e-06], 0.003995088200746315)
    # >> ([-2.2351742458681656e-05, 1.937151012754157e-05], 0.005466828316635659)
    # >> ([-1.639127780350691e-05, 1.3411045472366823e-05], 0.005559126764660465)
    # >> ([1.3411045472366823e-05, 1.3411045472366823e-05], 0.005744871050839639)
    # >> ([-1.639127780350691e-05, -4.470348493157417e-06], 0.0059305331781045354)
    # >> ([3.725290409306581e-05, -1.0430813148332163e-05], 0.00666362185897328)
    # >> ([-3.427267176903115e-05, -2.8312207113856402e-05], 0.007955898437553897)
    # >> ([2.5331974782716316e-05, -4.0233136424205895e-05], 0.008756371273953316)
    # >> ([-5.8114530389730135e-05, -1.0430813148332163e-05], 0.012011002599358011)
    # >> ([-0.00021308661142427354, -6.407499504490488e-05], 0.015151226345009473)
    #
    # HIT
    # RATE / MEDIAN - comparisons
    # ** ** *f1 ** ** *
    # FP:
    # (10, 0.0)
    # B:
    # (0, 6.709702757475879e-06)
    # ** ** *f3 ** ** *
    # FP:
    # (8, 1.2685883837875243e-07)
    # B:
    # (0, 0.10620053578081098)
    # ** ** *f6 ** ** *
    # FP:
    # (6, 0.0)
    # B:
    # (6, 9.32186378088673e-09)
    # ** ** *f7 ** ** *
    # FP:
    # (10, 4.048201256997297e-46)
    # B:
    # (0, 0.006297077518538908)



def zad2_load_params():
    fp_solve_yn, b_solve_yn = False, False

    with open("zad2_params.txt", "r") as params_file:
        lines = params_file.readlines()
        lines = [l.split("=") for l in lines]
        for l in lines:
            if l[0].strip() == "MAX_NUM_EVAL":
                MAX_NUM_EVAL = int(float(l[1].strip()))
            elif l[0].strip() == "F_LIST":
                f_num_list = l[1].strip().split()
            elif l[0].strip() == "N_VAR_LIST":
                N_test_dim_list = l[1].strip().split()
                N_test_dim_list = [int(el) for el in N_test_dim_list]
            elif l[0].strip() == "FP":
                fp_solve_yn = int(l[1]) == 1
            elif l[0].strip() == "B":
                b_solve_yn = int(l[1]) == 1
            elif l[0].strip() == "POP_SIZE":
                POP_SIZE = int(l[1].strip())
            elif l[0].strip() == "EXPLICIT_CONSTRAINT":
                EXPLICIT_CONSTRAINT = l[1].strip().split()
                EXPLICIT_CONSTRAINT = tuple([float(el) for el in EXPLICIT_CONSTRAINT])
            elif l[0].strip() == "P_MUTATION_FP":
                P_MUTATION_FP = float(l[1].strip())
            elif l[0].strip() == "P_MUTATION_B":
                P_MUTATION_B = float(l[1].strip())
            else: # N_DECIMAL
                N_DECIMAL = int(l[1].strip())

        return MAX_NUM_EVAL, f_num_list, N_test_dim_list, fp_solve_yn, b_solve_yn, POP_SIZE, EXPLICIT_CONSTRAINT, P_MUTATION_FP, P_MUTATION_B, N_DECIMAL


def zad2():
    print()
    print()
    print("************************* ZAD 2 *************************")

    MAX_NUM_EVAL, f_num_list, N_test_dim_list, fp_solve_yn, b_solve_yn, POP_SIZE, EXPLICIT_CONSTRAINT, P_MUTATION_FP, P_MUTATION_B, N_DECIMAL = zad2_load_params()

    f_goal_list = []
    if "f1" in f_num_list:
        f_goal_list.append(f1_goal)
    if "f3" in f_num_list:
        f_goal_list.append(f3_goal)
    if "f6" in f_num_list:
        f_goal_list.append(f6_goal)
    if "f7" in f_num_list:
        f_goal_list.append(f7_goal)

    ga_type_list = []
    if fp_solve_yn:
        ga_type_list.append("FP")
    if b_solve_yn:
        ga_type_list.append("B")


    f_goal_dict = dict(zip(f_goal_list, f_num_list))


    f_sol_list = []

    for fx_goal in f_goal_list:
        print()
        print("***** " + f_goal_dict[fx_goal] + " *****")
        f_sol_N_dim_list = []
        for N_dim in N_test_dim_list:
            print("N = ", N_dim)
            fx_list = []

            n_var = N_dim

            if fp_solve_yn:
                print("FP...")
                fp_list = []
                for i in range(GA_REPEAT_TIMES):
                    if print_yn:
                        print()
                        print("%5d run:" % (i + 1))
                    sol = run3TournamentEliminationSelection(sol_form="FP", max_f_eval=MAX_NUM_EVAL, f_goal=fx_goal, pop_size=POP_SIZE,
                                                             explicit_constraint=EXPLICIT_CONSTRAINT, n_var=n_var,
                                                             pm=P_MUTATION_FP)
                    print(i, "SOL =", sol)
                    fx_goal.reset_called_times()
                    fp_list.append(sol)

                fp_list.sort(key=itemgetter(1))
                hit_rate, median = get_hit_rate_and_median(fp_list)
                print(":: hit rate =", hit_rate)
                print(":: median =", median)
                fx_list.append((hit_rate, median))
                for sol in fp_list:
                    print(">>",sol)
                print()

            if b_solve_yn:
                print("B...")
                b_list = []
                for i in range(GA_REPEAT_TIMES):
                    if print_yn:
                        print()
                        print("%5d run:" % (i + 1))

                    sol = run3TournamentEliminationSelection(sol_form="B",max_f_eval=MAX_NUM_EVAL, f_goal=fx_goal, pop_size=POP_SIZE,
                                                             explicit_constraint=EXPLICIT_CONSTRAINT, n_var=n_var,
                                                             pm=P_MUTATION_B, num_decimal=N_DECIMAL)
                    print(i, "SOL =", sol)
                    fx_goal.reset_called_times()
                    b_list.append(sol)
                b_list.sort(key=itemgetter(1))
                hit_rate, median = get_hit_rate_and_median(b_list)
                print(":: hit rate =", hit_rate)
                print(":: median =", median)
                fx_list.append((hit_rate, median))
                for sol in b_list:
                    print(">>",sol)
                print()

            f_sol_N_dim_list.append(fx_list)
        f_sol_list.append(f_sol_N_dim_list)

    print("")
    print("HIT RATE / MEDIAN - comparisons")
    for i in range(len(f_num_list)):
        print("***** " + f_num_list[i] + " *****")
        f_N_dim_vals_list = f_sol_list[i]
        for k in range(len(f_N_dim_vals_list)):
            print("N =", N_test_dim_list[k], "------")
            fx_vals = f_N_dim_vals_list[k]
            for j in range(len(ga_type_list)):
                print("  "+ga_type_list[j]+":")
                print(fx_vals[j])
        print()

    #
    # ** ** *f6 ** ** *
    # N = 1
    # FP...
    # :: hit rate = 10
    # :: median = 0.0
    # >> ([1.408378763146142e-11], 0.0)
    # >> ([-8.394921271865444e-10], 0.0)
    # >> ([-1.912935654679704e-09], 0.0)
    # >> ([7.928283599074525e-10], 0.0)
    # >> ([-9.7090881641425e-10], 0.0)
    # >> ([-1.3597548172320981e-09], 0.0)
    # >> ([-7.819266944022866e-10], 0.0)
    # >> ([1.8808107770549223e-09], 0.0)
    # >> ([2.038098756352137e-10], 0.0)
    # >> ([-1.4709369134423177e-09], 0.0)
    #
    # B...
    # :: hit rate = 10
    # :: median = 2.000399845769607e-11
    # >> ([1.4901161620173298e-06], 2.2226664952995634e-12)
    # >> ([1.4901161620173298e-06], 2.2226664952995634e-12)
    # >> ([-4.470348493157417e-06], 2.000399845769607e-11)
    # >> ([-4.470348493157417e-06], 2.000399845769607e-11)
    # >> ([-4.470348493157417e-06], 2.000399845769607e-11)
    # >> ([-4.470348493157417e-06], 2.000399845769607e-11)
    # >> ([-4.470348493157417e-06], 2.000399845769607e-11)
    # >> ([-4.470348493157417e-06], 2.000399845769607e-11)
    # >> ([-4.470348493157417e-06], 2.000399845769607e-11)
    # >> ([-4.470348493157417e-06], 2.000399845769607e-11)
    #
    # N = 3
    # FP...
    # :: hit rate = 0
    # :: median = 0.009715909877514362
    # >> ([-0.06994270699668895, -3.0529795296933164, 0.7242313158829314], 0.009715909877514362)
    # >> ([3.068348448640285, -0.655428022985405, 0.07575542369299273], 0.009715909877514362)
    # >> ([2.182559477161255, 2.187555108795962, -0.5487474440270633], 0.009715909877514362)
    # >> ([1.521546393174157, 2.6021720326132023, 0.8738903033715657], 0.009715909877514362)
    # >> ([0.4689641322770966, 2.510720624760703, 1.8238534985457795], 0.009715909877514362)
    # >> ([-2.403149204871367, 1.6759173382078074, 1.1252830533442728], 0.009715909877514362)
    # >> ([-0.5043421863342691, 2.876637248880517, 1.1492101929657805], 0.009715909877514362)
    # >> ([-1.6647493052463802, -1.8778802533090146, 1.884744670798518], 0.009715909877514362)
    # >> ([2.6041123265201334, 1.6384831117242276, 0.6197249787704602], 0.009715909877514362)
    # >> ([0.0644516431993929, -2.9961673435980916, -0.9321556830947736], 0.009715909877514362)
    #
    # B...
    # :: hit rate = 0
    # :: median = 0.009715934835106521
    # >> ([1.1679426183683432, 2.5073603542852467, -1.4829501951620045], 0.009715909877515527)
    # >> ([1.9450501187160611, 1.6313925871668005, 1.8454626752574015], 0.009715912229244728)
    # >> ([-2.8230235821909773, -1.106946203319616, -0.8091554286824305], 0.009715917758090165)
    # >> ([-2.327953348396818, -1.1807367557506794, -1.742403261137106], 0.009715919201668366)
    # >> ([2.0254269845910926, -0.31589717614344437, -2.3764000051140783], 0.009715920878075601)
    # >> ([3.117074165257037, -0.34880490150466414, 0.11627823460931097], 0.009715948792137441)
    # >> ([-2.282927998391628, -1.6274676211913714, 1.4099851670856793], 0.00971597507766575)
    # >> ([2.831710959425891, 0.40493757739476877, -1.2920007494688264], 0.009715985326784415)
    # >> ([-2.4505899086770384, -1.500497803106839, 1.2629226226485528], 0.009715988018386468)
    # >> ([2.351005445450703, -2.0369932662544628, 0.41901023444563634], 0.00971601463969246)
    #
    # N = 6
    # FP...
    # :: hit rate = 0
    # :: median = 0.03722407510748782
    # >> ([2.1094076278036145, -0.6985509233282912, 1.4971771699640972, 0.9830246290225341, -1.3044402070226628,
    #      -0.05542479882293308], 0.009715909877514362)
    # >> ([1.4440346604771528, 0.2690795610666407, -0.5568874318373309, -1.1659170501692946, 1.1775547476166115,
    #      2.153212879501966], 0.009715909877514362)
    # >> ([2.6101190364504676, 0.45403244572198254, -0.30644851506145904, 0.6819337702898804, 0.6144691995941374,
    #      1.376482251687496], 0.009715909877514362)
    # >> ([0.6897205468254968, 4.793385302928889, 1.1456723719076964, 2.0613135301179173, -0.8432558468516567,
    #      -3.1108923044319745], 0.03722407510748782)
    # >> ([3.0970582458393743, 0.07639076861382244, -3.27138753591528, 1.5017992223993373, -3.9380421900006093,
    #      1.1573280291089434], 0.03722407510748782)
    # >> ([1.7685639664408406, -3.993968127191491, -1.086159879167641, 0.5551195197090067, 4.254114129772888,
    #      0.8588243094468161], 0.03722407510748782)
    # >> ([-1.4009580888140392, -5.062486047052968, -0.39893205645132523, -1.916388242556319, 1.935363542559685,
    #      -2.0576189726822447], 0.03722407510748782)
    # >> ([1.518236191857912, 0.349339152892914, -5.001674138839526, 1.074426179349006, -3.219308931767796,
    #      -0.6636281641316363], 0.03722407510748782)
    # >> ([2.6116618494123864, 2.018044077982839, 0.3804857807342351, -2.527822159936162, 4.6852326014572725,
    #      -0.15240980702071122], 0.03722407510748782)
    # >> ([3.4421551126866317, 4.103439883322818, -0.5297530923331133, -1.271973600361311, 0.7821706566775242,
    #      -2.864536668832197], 0.03722407510748782)
    #
    # B...
    # :: hit rate = 0
    # :: median = 0.07848786239524996
    # >> ([2.5466398163628483, -1.233868337686907, 4.264600702065245, -0.6951496212229031, -0.8852944041876327,
    #      3.420300883659742], 0.03757091422399733)
    # >> ([-2.649532337472806, 2.724094770076711, -3.1183228826022997, 1.6930357126306177, -3.348665039201535,
    #      0.807024562568202], 0.038743282612831165)
    # >> ([3.1689004054337815, 1.442374332021899, 2.9933766422681956, 0.36720187566285745, -4.349373410623478,
    #      0.06376654099722145], 0.04037300814703526)
    # >> ([0.42787940585253637, 1.7932788072013466, 0.3882899996128657, -3.9506183251922877, -2.211676603903669,
    #      3.7732915214685008], 0.044645013847837534)
    # >> ([2.3592845308567405, -3.8273678370525843, -0.7858321304867317, 8.062882961716745, 0.3140643332619746,
    #      -1.561997877418932], 0.07837949595319676)
    # >> ([3.2544092313769184, 6.399120581123853, 0.6201997584164047, 4.335267971016997, -0.09122938189594265,
    #      4.28370399128508], 0.07859622883730316)
    # >> ([5.6755857370968315, 5.706854334677878, 1.0619357544760604, -3.790657335241363, 2.157266502298903,
    #      -1.7242417253327886], 0.07956900614549384)
    # >> ([5.965365647237462, 1.5766755216322963, -1.6127929572103312, 3.2576934474019197, 4.359777401679075,
    #      -4.108081880452694], 0.08355039592042962)
    # >> ([-3.5791919702050734, 1.8438831521237802, -0.29971451460464493, 3.93323463002546, -2.0894036319674143,
    #      0.2657249649085074], 0.09867438870142564)
    # >> ([-2.167908911940721, 2.945770411067315, 0.7063642354716109, -1.9696459761156433, 8.583130198214363,
    #      1.1149362061898742], 0.11482836774180061)
    #
    # N = 10
    # FP...
    # :: hit rate = 0
    # :: median = 0.12699051873930803
    # >> ([1.1786919808646625, 1.6454789734511974, 0.2215686996937314, -3.2066342533014516, 1.82726042577827,
    #      -4.6792571245364, -0.6285599138126845, 6.272029049498371, -0.17141579082815606, -3.039381030643865],
    #     0.07818918214340193)
    # >> ([1.5611207798266507, -6.087575842386515, 1.731423600598729, 1.7714348113386111, 3.5329662369120416,
    #      -2.6702479435179223, -0.4618448413214508, 3.3208814679283374, 3.1911131588006687, -1.4125074093779442],
    #     0.07818918214340193)
    # >> ([2.948757942138791, -1.6460703180144312, 5.059249174178272, 5.790274733203646, 0.05277167768392025,
    #      -2.345014608612324, -8.440944436817222, 0.56175270757454, 3.0764799979656328, -0.7625779960515354],
    #     0.126990518739308)
    # >> ([2.1447246744237756, -3.7494551404194856, -0.23015374173674977, -0.894008055712201, -3.2599744138217255,
    #      4.63665928367673, -7.70946377988769, -1.2028260672141682, 6.1306589756997765, -2.745382155245453],
    #     0.126990518739308)
    # >> ([-3.4747737944430677, -0.6226615529926518, 0.03900059079621708, 0.055124706768428317, 5.878525853200553,
    #      2.102275152110925, 0.8161612913990328, 0.5874454361306615, 10.077708636332524, -1.904436028310366],
    #     0.126990518739308)
    # >> ([1.8440451109981197, -2.088445423696505, -0.6021526822494434, 9.32968785283567, -7.265412793499207,
    #      0.13584552954674473, -0.7575418257707753, 0.8967242884925779, -0.7578736321747882, -2.7778486103468936],
    #     0.12699051873930806)
    # >> ([-0.1203639833983217, 0.4354173875558158, -7.128395477463744, 3.79434434465478, 9.0370334282509,
    #      0.28140138444297813, 0.7564444139222134, 0.30072984736261027, 2.7806400827974613, -1.443714408080282],
    #     0.1269905187399089)
    # >> ([-1.0786494700909677, 7.005906064856253, 8.823858310978544, 3.4283929162861435, -2.790554002412284,
    #      -8.289543152618874, 1.7792898358468094, 0.7634595472984925, 0.5483906490189991, 5.092246776549271],
    #     0.17822230254550497)
    # >> ([-0.41253900240766034, 4.978181367871105, 6.432896615831526, -9.980136289726815, -3.009691737080779,
    #      5.236258154631023, -3.556464345519456, 4.726537761288197, 2.982713024039464, -0.21248870884211277],
    #     0.17822230254550497)
    # >> ([-6.164431544126299, 2.409908190228038, -2.8254336411020486, 8.014247315472455, -6.5560406269988585,
    #      5.527695993847258, -4.088571192942163, 5.9408623603201285, 0.002836226575276559, 2.185922879574036],
    #     0.17822230254550508)
    #
    # B...
    # :: hit rate = 0
    # :: median = 0.31848483358869945
    # >> ([-6.641797919326962, -2.275084026905418, 2.724845788623263, -0.12065619589854037, 6.428583157914382,
    #      3.9418176991289116, -4.832385624420219, -9.483127578590143, -0.2212926811365108, -3.0141756836824314],
    #     0.24773605432136558)
    # >> ([9.081758829407661, -7.847766812079158, -11.818711811861746, -3.830431515885344, -8.358590553956944,
    #      -8.154700939497381, -4.325692484548469, 5.250151611869079, -1.089309188404954, 0.48113019708186755],
    #     0.27279548697149086)
    # >> ([6.8730848095740384, -6.828002835154621, 1.4987364858012313, -5.445598228144597, -5.859576340305104,
    #      9.583004104584582, -7.683174541091162, -11.123912367937336, 7.167931114671568, 0.5012229234344616],
    #     0.2753999695138739)
    # >> ([-13.125984761893292, -2.515922561762409, -0.1919552741037407, -1.0835513795480551, 7.706929972974365,
    #      -5.014948696343566, 9.351314882973284, -2.0662114639941294, -10.815041089506181, 1.3854638154942904],
    #     0.2878047842748078)
    # >> ([-5.371438126904913, 2.0980373650204314, -8.55597730147771, -2.0150587563234197, -9.747492067441108,
    #      13.268711068293783, -6.22596014219404, 13.312484720721386, -3.8163827006930973, -4.899059382053004],
    #     0.31617973259956844)
    # >> (
    # [4.883439984424108, -8.777515851781246, 4.282654949505769, -7.536988184958346, 14.078899147477713, 9.27375731688015,
    #  1.9936338661203905, -11.329315940419313, 5.9654490937426345, -3.227888143893729], 0.32078993457783045)
    # >> ([2.0052567721979813, -16.568409400236888, -7.761948041973952, 12.49453611655641, -4.5740233234770145,
    #      4.923518148765503, -0.653259475626335, -12.237887449201565, 1.743151299451327, 10.13481200143135],
    #     0.34613637396894226)
    # >> ([-3.9921568033742005, -9.701584568666952, 15.897901830014632, -4.6128736320994435, 13.040991516142832,
    #      -5.370484452560085, -1.5838906640973889, 4.605658489634344, -11.226962841360653, 7.549377010744131],
    #     0.3530380896666079)
    # >> ([-14.07910478350832, 3.9174632405478675, -1.883409973484575, -0.6504878595616788, -1.4907464829309731,
    #      10.624166149621196, 17.797507876083486, -13.0938578574019, 13.315846422786905, -1.1989221334136175],
    #     0.3848441179367483)
    # >> ([9.917338488022644, 12.440236283547769, -11.993037521631642, -8.418773365580243, 6.723119518849842,
    #      -1.2839839245076163, -3.2149002914101033, -11.918460187866096, -15.109776410751827, -10.53325416246814],
    #     0.39839795824489205)
    #
    # ** ** *f7 ** ** *
    # N = 1
    # FP...
    # :: hit rate = 10
    # :: median = 2.03787921110552e-44
    # >> ([3.153203962679443e-90], 1.7757263197574798e-45)
    # >> ([-1.090805497821507e-89], 3.302734469831789e-45)
    # >> ([-2.971970463722902e-89], 5.4515781785854466e-45)
    # >> ([-9.004629534482303e-89], 9.489272645720695e-45)
    # >> ([-3.3591513881481695e-88], 1.8327987855048817e-44)
    # >> ([-5.030867931893022e-88], 2.2429596367061585e-44)
    # >> ([-6.031121691316885e-88], 2.455834214949553e-44)
    # >> ([1.0517127384962927e-85], 3.2430120852323273e-43)
    # >> ([2.866413654562358e-85], 5.353889851838903e-43)
    # >> ([2.9249676085932104e-85], 5.408296967246908e-43)
    #
    # B...
    # :: hit rate = 0
    # :: median = 0.003818587009648561
    # >> ([1.4901161620173298e-06], 0.0013108196020125559)
    # >> ([1.4901161620173298e-06], 0.0013108196020125559)
    # >> ([-4.470348493157417e-06], 0.003818587009648561)
    # >> ([-4.470348493157417e-06], 0.003818587009648561)
    # >> ([-4.470348493157417e-06], 0.003818587009648561)
    # >> ([-4.470348493157417e-06], 0.003818587009648561)
    # >> ([-4.470348493157417e-06], 0.003818587009648561)
    # >> ([-4.470348493157417e-06], 0.003818587009648561)
    # >> ([-4.470348493157417e-06], 0.003818587009648561)
    # >> ([-4.470348493157417e-06], 0.003818587009648561)
    #
    # N = 3
    # FP...
    # :: hit rate = 6
    # :: median = 4.883017744709409e-10
    # >> ([5.848139731500477e-30, 1.2601075775266502e-30, 2.029991416801918e-30], 2.513442792205493e-15)
    # >> ([-2.2377595243704158e-28, -2.509987194407105e-28, -1.5409884387043377e-28], 1.9232661244683948e-14)
    # >> ([-3.2150767447264964e-28, -1.5294770702140814e-28, 8.870136845367796e-28], 3.091601496748232e-14)
    # >> ([1.0201338858895923e-27, 1.052246256137351e-27, -1.9325593487647967e-27], 4.9248595383574085e-14)
    # >> ([-8.28735005945354e-27, -4.9366853580838745e-26, -7.158520651526198e-26], 2.9555220233910945e-13)
    # >> ([-6.832017443355194e-19, 3.160145839128094e-19, -5.842560106189013e-19], 9.763079967395426e-10)
    # >> ([1.210646792553737e-05, 2.1683346350906464e-05, -8.151768149281567e-06], 0.005364481906723688)
    # >> ([-6.282659918072612e-06, 1.2407704082143992e-05, 2.2130577367533625e-05], 0.005364481906723688)
    # >> ([-6.235199494058547e-06, -1.608693883966641e-05, 1.963461434168953e-05], 0.005364481906723688)
    # >> ([0.0007053151139985157, -0.0001359455251156633, -0.0006409851481403664], 0.031349487311277756)
    #
    # B...
    # :: hit rate = 0
    # :: median = 0.17857966091442168
    # >> ([0.0005677342584036182, -0.004808604860564003, -0.005351007144184905], 0.0883498790504692)
    # >> ([-0.005845725710564409, -0.0036761165760808012, -0.0018283725329766298], 0.08926801340632078)
    # >> ([-0.005988776862288603, 0.010146200959269436, -0.011633336890739088], 0.12876467396158917)
    # >> ([0.001914799270473111, -0.017820299202810475, 4.91738334034153e-05], 0.1521229980668684)
    # >> ([-0.007776916258841027, -0.017057359726948107, -0.0001952052174587493], 0.17807899192562038)
    # >> ([-0.021587312864880914, -0.00924915102866919, -0.020949543146777216], 0.179080329903223)
    # >> ([0.008524954573061905, 0.011511147365304453, 0.029964745937725468], 0.18768321296862447)
    # >> ([-0.022809208119191737, 0.011731684557545918, 0.022174418633412074], 0.198044090908676)
    # >> ([-0.04131049040885415, -0.03695339074592141, -0.015847385401947633], 0.24017121125342897)
    # >> ([-0.058494510009722944, 0.0767603539455024, -0.0012919307140109026], 0.3132356461334543)
    #
    # N = 6
    # FP...
    # :: hit rate = 0
    # :: median = 0.663778036864906
    # >> ([-0.030517155916604914, -0.11753973721270797, -0.035513125606819865, 0.0594179197906813, 0.06743571935246725,
    #      -0.022951123452874603], 0.3966084293340289)
    # >> ([0.006005964111384811, -0.060166754132775405, 0.07029689782449211, -0.11691019982907015, 0.04761481855392101,
    #      -0.008941962676465708], 0.3966084293340289)
    # >> ([0.06835785073909087, -0.10248812063649461, 0.02999969071736798, 0.06675755930983544, -0.17810579955949718,
    #      0.0812379905092915], 0.49308802971103377)
    # >> ([-0.12115821716798435, -0.13066890091880914, -0.034382510202307114, 0.05932729052998813, -0.30515789681883526,
    #      0.040418379346365794], 0.6024216794399698)
    # >> ([0.09070973039769889, 0.019958550267774496, 0.2720454363211028, -0.010063587443781894, -0.20738249360734123,
    #      -0.0739354747235699], 0.6024216794399699)
    # >> ([-0.22014022344993422, 0.13944829982638807, 0.05878019136590336, -0.038210935109222285, -0.23687875971777497,
    #      0.3829588148384925], 0.725134394289842)
    # >> ([-0.1552528184799645, -0.12741342085342386, -0.8136595507626133, -0.5431364801383, 0.08744143595187781,
    #      0.21011385252851172], 1.0126999330046977)
    # >> ([0.7952922151846075, 0.2082051087374131, -0.16353851392070679, 0.08571154392321298, 0.2491660645835332,
    #      0.5264564909667114], 1.0126999330046977)
    # >> ([-0.09596556867283561, -0.3714534610001822, -0.6456357560481155, -0.19670942959862733, -0.08471332615171537,
    #      0.6627722983473044], 1.0126999330049564)
    # >> ([-0.27553557461589323, -0.8099830607768412, -0.04565059478618738, 1.408713914515451, 0.584621753164513,
    #      -0.591785794795938], 1.359623170195214)
    #
    # B...
    # :: hit rate = 0
    # :: median = 2.229972291157847
    # >> ([0.31449944718180234, -1.0771617614377078, 0.21092445286883077, -2.0687744637958545, 0.2114608946877965,
    #      0.6816104257586701], 1.6414290050570723)
    # >> ([1.8380240153677434, 0.44731052062840604, 0.5095675739517063, 1.2083307268718073, -1.3351127903197053,
    #      1.7139092598530397], 1.8071080705100793)
    # >> ([0.8744552992121939, 1.1790052407683476, 0.0444248331911794, -0.18844456042184277, 0.4969969539939427,
    #      0.7058993192285072], 2.060868927263305)
    # >> ([-0.7586762535177556, 3.459216757393378, 0.6980672686716076, -1.3744101337912724, 1.1253610588717748,
    #      -0.7345542530582634], 2.192754481396589)
    # >> ([0.9012893110897906, -0.7725164524470713, 0.2950087277593809, 1.5535608396995286, 0.2550736145697101,
    #      -3.34303836056705], 2.213770337840769)
    # >> ([1.8593565183686138, -1.6269013770491298, 2.3889855262334763, -2.345584402846825, 2.7929202554500137,
    #      -0.4899606254685125], 2.2461742444749246)
    # >> ([1.8421665383030899, 1.7144874249245916, -0.3698214998788103, 2.8555110947940037, -2.4913475659891233,
    #      -2.112518313900182], 2.2477701626231137)
    # >> ([3.678639342744326, 0.8786097430768507, 2.261950143037737, -0.9629652191092148, -0.20286888488736565,
    #      -2.2764251314528323], 2.2504357683792557)
    # >> ([-1.8826410735440575, 4.692681273599895, 0.6775394283991858, -3.0192241972453644, 2.041198374068685,
    #      0.47438295109220974], 2.510621113804743)
    # >> ([-1.925943849263902, -1.5831396455508369, -1.457451327367167, 0.5801394754689753, -5.520545110718764,
    #      -1.5264317848215043], 2.778925026293801)
    #
    # N = 10
    # FP...
    # :: hit rate = 0
    # :: median = 3.2765421527013268
    # >> ([-0.1758517588795769, -0.8519911634770799, 2.058666174111134, -3.9146205135506382, 0.763433100156588,
    #      -0.9736290924845994, 1.4202078818789972, -0.031288015403948835, -1.0670565456983427, 0.6297920267687921],
    #     2.2457646481814764)
    # >> ([-0.25549431329894773, 1.3503722908700584, 2.7158197609443073, -1.9360101851846077, 5.22314693195602,
    #      3.515872517253934, 2.446533691979508, -1.2562578243207625, -5.426477683389897, 1.2658247434021992],
    #     3.0916451349719463)
    # >> ([5.409979757761082, 4.650222737973097, -0.45054158061376465, -3.5440623045569297, 1.7110045270605307,
    #      1.4937124683746725, 0.19296076262073053, 0.7186816731548701, 0.4952813485279078, -4.653421276205483],
    #     3.0916451349719476)
    # >> ([1.6908907294190574, 1.2630009966014002, -4.9898345468454215, 2.0109003810831694, -0.7945712212996745,
    #      0.35394022088557603, 1.7169593657226232, 3.234357260413216, -2.627767599097361, -6.066354404892649],
    #     3.0916451349720626)
    # >> ([0.14436140138089115, 7.014645206703097, 3.003047325012795, -2.591441455245732, 3.84628174762948,
    #      1.1530451112447546, 2.317703523437335, -0.8113255875206575, -0.6288710215929577, 1.9430014969442],
    #     3.091651114414713)
    # >> ([3.8616531619655214, 6.910410469664386, -0.028239906027047623, 4.7671507416178365, 1.1544428274322311,
    #      4.32216176610491, 0.054301784249379384, -3.9740441003440443, -3.266215308115569, -2.300101404173649],
    #     3.4614331909879406)
    # >> ([2.3961308237166707, 10.249572006779413, 7.5727108126579665, 7.275762458885255, 0.8307300774035422,
    #      -1.1144587899901985, 3.1478116144760913, -2.5160571942991226, -5.2353280879915625, 3.4871085341987245],
    #     4.194001050546978)
    # >> ([-6.621918717705155, 13.066833722575089, -7.387227084194687, 2.7491206302894113, 0.9992743135948502,
    #      5.605008204252186, -0.03889591567096495, -9.209946344546504, 0.11363463568032817, 1.7429627877287925],
    #     4.558529405688354)
    # >> ([0.6845866032635151, 20.513372165044302, 10.797366486402444, 1.3560004353021302, -7.947616058068185,
    #      -9.822414085030267, -8.687065633259142, 2.2472348403240527, -0.5954050843063111, 2.1791606908284695],
    #     5.293980544219155)
    # >> ([6.799374364186083, -6.20587559888461, 18.291246685007543, -1.6896552415210504, 1.0689737409337117,
    #      -1.7607135575924282, -3.6044648921051117, -9.193406954708726, -11.408719726686, 11.439850239585766],
    #     5.293980544219155)
    #
    # B...
    # :: hit rate = 0
    # :: median = 5.410428322487179
    # >> ([-0.66161604707289, -9.979968070386889, 0.2736464224352346, -0.3896221634633008, -1.002638071854058,
    #      7.783134513590774, -8.473204328811299, 5.560310350665752, -2.7200565552728335, 3.982593237834962],
    #     4.174380792627839)
    # >> ([6.525506273672178, 8.252348251710785, 13.373257618345548, -1.7969593941259205, -12.006174385731647,
    #      -3.978429853273333, 3.0483500077828722, -4.5475886627313145, 0.8886352446268546, 8.735581002699767],
    #     4.920565217722959)
    # >> ([6.399120581123853, -3.0215785807841584, -3.7042782814585706, 11.791093998881998, -0.6204232758409773,
    #      -2.4258718617520287, 17.46813259923853, 5.532385573756258, -3.0305848428781275, 4.879231896377554],
    #     5.079561437149794)
    # >> ([-6.199877148862996, -4.747276149608979, 0.32179505591973623, -11.198680436571848, -12.138848368491182,
    #      -4.163257901765647, -8.256547399060352, -10.534607187944864, -10.897963073788972, -11.816011721372952],
    #     5.305415757817742)
    # >> ([10.362835537279715, -16.738574705677472, 8.778764569126508, 3.5578564869718647, -2.750055573882328,
    #      -5.177490567490182, -14.330904612866178, -2.1694645932157215, -5.847774620287858, 5.914606330233994],
    #     5.322842399238268)
    # >> ([14.196254736073456, -9.853838677818736, -6.814460659458064, -4.011128962371622, 6.027306795934038,
    #      1.7273322262564932, 6.691928407309312, 13.94021701634577, -8.27810639971812, -7.809220487154143],
    #     5.49801424573609)
    # >> ([5.375327330092404, -0.38567037539691995, -2.228961951403676, 10.476555242435794, 2.872850086475907,
    #      -11.90686708411178, 14.188768392466557, 14.5392735165141, -2.8505549684332294, -7.328580538290161],
    #     5.517609286228683)
    # >> ([-10.748873971369086, 11.214675343474013, -6.5022872836079415, 3.745438270134869, -7.899891075488661,
    #      -4.428772760295061, 0.436700893542195, 11.030532748417045, 13.170464729382537, -11.487429186327134],
    #     5.672405566276742)
    # >> ([1.274730303130454, -5.425958497105796, -8.315055320115547, -1.260469891442952, -12.478118016663728,
    #      -14.621846813614567, 5.144150708441451, -8.250193543737936, -16.28402371060919, 15.83908992526203],
    #     5.737501451884306)
    # >> ([-14.879326965788806, 16.3619895685312, -6.035636545289655, 2.653427501124959, 2.5370911519852584,
    #      5.1907913443681934, 6.07854295010992, 10.419620884049564, -18.485485568210052, -5.745940081654197],
    #     5.76187646275562)
    #
    # HIT
    # RATE / MEDIAN - comparisons
    # ** ** *f6 ** ** *
    # N = 1 - -----
    # FP:
    # (10, 0.0)
    # B:
    # (10, 2.000399845769607e-11)
    # N = 3 - -----
    # FP:
    # (0, 0.009715909877514362)
    # B:
    # (0, 0.009715934835106521)
    # N = 6 - -----
    # FP:
    # (0, 0.03722407510748782)
    # B:
    # (0, 0.07848786239524996)
    # N = 10 - -----
    # FP:
    # (0, 0.12699051873930803)
    # B:
    # (0, 0.31848483358869945)
    #
    # ** ** *f7 ** ** *
    # N = 1 - -----
    # FP:
    # (10, 2.03787921110552e-44)
    # B:
    # (0, 0.003818587009648561)
    # N = 3 - -----
    # FP:
    # (6, 4.883017744709409e-10)
    # B:
    # (0, 0.17857966091442168)
    # N = 6 - -----
    # FP:
    # (0, 0.663778036864906)
    # B:
    # (0, 2.229972291157847)
    # N = 10 - -----
    # FP:
    # (0, 3.2765421527013268)
    # B:
    # (0, 5.410428322487179)


def zad3_load_params():
    fp_solve_yn, b_solve_yn = False, False

    with open("zad3_params.txt", "r") as params_file:
        lines = params_file.readlines()
        lines = [l.split("=") for l in lines]
        for l in lines:
            if l[0].strip() == "MAX_NUM_EVAL":
                MAX_NUM_EVAL = int(float(l[1].strip()))
            elif l[0].strip() == "F_LIST":
                f_num_list = l[1].strip().split()
            elif l[0].strip() == "N_VAR_LIST":
                N_test_dim_list = l[1].strip().split()
                N_test_dim_list = [int(el) for el in N_test_dim_list]
            elif l[0].strip() == "FP":
                fp_solve_yn = int(l[1]) == 1
            elif l[0].strip() == "B":
                b_solve_yn = int(l[1]) == 1
            elif l[0].strip() == "POP_SIZE":
                POP_SIZE = int(l[1].strip())
            elif l[0].strip() == "EXPLICIT_CONSTRAINT":
                EXPLICIT_CONSTRAINT = l[1].strip().split()
                EXPLICIT_CONSTRAINT = tuple([float(el) for el in EXPLICIT_CONSTRAINT])
            elif l[0].strip() == "P_MUTATION":
                P_MUTATION = float(l[1].strip())
            else: # N_DECIMAL
                N_DECIMAL = int(l[1].strip())

        return MAX_NUM_EVAL, f_num_list, N_test_dim_list, fp_solve_yn, b_solve_yn, POP_SIZE, EXPLICIT_CONSTRAINT, P_MUTATION, N_DECIMAL

def zad3():
    print()
    print()
    print("************************* ZAD 3 *************************")

    MAX_NUM_EVAL, f_num_list, N_test_dim_list, fp_solve_yn, b_solve_yn, POP_SIZE, EXPLICIT_CONSTRAINT, P_MUTATION, N_DECIMAL = zad3_load_params()

    f_goal_list = []
    if "f1" in f_num_list:
        f_goal_list.append(f1_goal)
    if "f3" in f_num_list:
        f_goal_list.append(f3_goal)
    if "f6" in f_num_list:
        f_goal_list.append(f6_goal)
    if "f7" in f_num_list:
        f_goal_list.append(f7_goal)

    ga_type_list = []
    if fp_solve_yn:
        ga_type_list.append("FP")
    if b_solve_yn:
        ga_type_list.append("B")

    f_goal_dict = dict(zip(f_goal_list, f_num_list))


    f_sol_list = []

    for fx_goal in f_goal_list:
        print()
        print("***** " + f_goal_dict[fx_goal] + " *****")
        f_sol_N_dim_list = []
        for N_dim in N_test_dim_list:
            print("N = ", N_dim)
            fx_list = []

            n_var = N_dim

            if fp_solve_yn:
                print("FP...")
                fp_list = []
                for i in range(GA_REPEAT_TIMES):
                    if print_yn:
                        print()
                        print("%5d run:" % (i + 1))

                    sol = run3TournamentEliminationSelection(sol_form="FP", max_f_eval=MAX_NUM_EVAL, f_goal=fx_goal, pop_size=POP_SIZE,
                                                             explicit_constraint=EXPLICIT_CONSTRAINT, n_var=n_var,
                                                             pm=P_MUTATION)
                    print(i, "SOL =", sol)
                    fx_goal.reset_called_times()
                    fp_list.append(sol)

                fp_list.sort(key=itemgetter(1))
                hit_rate, median = get_hit_rate_and_median(fp_list)
                print(":: hit rate =", hit_rate)
                print(":: median =", median)
                fx_list.append((hit_rate, median))
                for sol in fp_list:
                    print(">>",sol)
                print()

            if b_solve_yn:
                print("B...")
                b_list = []
                for i in range(GA_REPEAT_TIMES):
                    if print_yn:
                        print()
                        print("%5d run:" % (i + 1))

                    sol = run3TournamentEliminationSelection(sol_form="B",max_f_eval=MAX_NUM_EVAL, f_goal=fx_goal, pop_size=POP_SIZE,
                                                             explicit_constraint=EXPLICIT_CONSTRAINT, n_var=n_var,
                                                             pm=P_MUTATION, num_decimal=N_DECIMAL)
                    print(i, "SOL =", sol)
                    fx_goal.reset_called_times()
                    b_list.append(sol)
                b_list.sort(key=itemgetter(1))
                hit_rate, median = get_hit_rate_and_median(b_list)
                print(":: hit rate =", hit_rate)
                print(":: median =", median)
                fx_list.append((hit_rate, median))
                for sol in b_list:
                    print(">>",sol)
                print()

            f_sol_N_dim_list.append(fx_list)
        f_sol_list.append(f_sol_N_dim_list)

    print("")
    print("HIT RATE / MEDIAN - comparisons")
    for i in range(len(f_num_list)):
        print("***** " + f_num_list[i] + " *****")
        f_N_dim_vals_list = f_sol_list[i]
        for k in range(len(f_N_dim_vals_list)):
            print("N =", N_test_dim_list[k], "------")
            fx_vals = f_N_dim_vals_list[k]
            for j in range(len(ga_type_list)):
                print("  "+ga_type_list[j]+":")
                print(fx_vals[j])
        print()

    ######### P_MUTATION = 0.1
    # ** ** *f6 ** ** *
    # N = 3
    # FP...
    # :: hit rate = 1
    # :: median = 0.009715909877514362
    # >> ([-1.2408060039891225e-09, 7.466412730357695e-10, 7.325567944229235e-10], 0.0)
    # >> ([0.8192274611565213, -1.4324421032274777, 2.6696559623931835], 0.009715909877514362)
    # >> ([-1.8471151976994056, -2.151661833102151, 1.3448434032580296], 0.009715909877514362)
    # >> ([-1.2820850513671058, -2.6787565047759885, 1.01518889129993], 0.009715909877514362)
    # >> ([1.4717965406255917, 2.6293501010865516, 0.8777357018431516], 0.009715909877514362)
    # >> ([-1.6763332876290042, 2.103042607949199, -1.6177779427407386], 0.009715909877514362)
    # >> ([1.1856558940036586, 2.8977234807689585, -0.2179580015139566], 0.009715909877514362)
    # >> ([0.32731855397736587, 0.214358152256838, 3.1140006636180857], 0.009715909877514362)
    # >> ([-2.8492285189927866, 0.4657725833881227, 1.2308694981079853], 0.009715909877514362)
    # >> ([-0.4349237037178955, -2.226732637966439, -2.168545618326425], 0.009715909877514362)
    #
    # B...
    # :: hit rate = 0
    # :: median = 0.00971591406479358
    # >> ([-2.4373781382456485, 1.1552100921679056, -1.6046293280741395], 0.009715910002570827)
    # >> ([1.3450867391046302, -2.835441987725254, 0.03187657922582332], 0.009715910042007059)
    # >> ([2.836634081189196, -1.3419872960983739, -0.0527143729755295], 0.009715910204937894)
    # >> ([2.7820838842791957, 0.5749943613979198, -1.3338810605435683], 0.00971591130099475)
    # >> ([2.623869239744778, -1.7116316373975948, -0.18985280506744573], 0.009715912586512387)
    # >> ([-3.0424847805427504, 0.74760949497675, -0.18689641327686957], 0.009715915543074771)
    # >> ([0.23777496231792838, -2.030063643485853, 2.381540480394598], 0.00971592061776777)
    # >> ([2.603079129733622, 1.745153305603651, -0.16639240569706004], 0.009715926610023629)
    # >> ([-1.624370395837019, 0.6696942661734937, -2.600742626544303], 0.009715927657546974)
    # >> ([1.5476472604976976, -0.23210059742956446, -2.720142707892755], 0.009715993122724642)
    #
    # N = 6
    # FP...
    # :: hit rate = 0
    # :: median = 0.07818918214340193
    # >> ([0.44057487438286613, 3.126801401997852, 2.2307303260033673, 0.6671525972339424, 1.5076681622316017,
    #      4.662323478728834], 0.03722407510748782)
    # >> ([-1.3044377945817947, -1.433204693374306, -0.033959990445780325, -5.812613850428832, 0.8015480497932057,
    #      -1.1030961409190623], 0.03722407510748782)
    # >> ([3.4547537825983294, 3.7209960256573797, -4.854107807730138, 3.767459218690381, 0.059943650661502916,
    #      5.012250596804694], 0.07818918214340193)
    # >> ([1.2232311213162361, 1.7712027233258019, 4.219030819812756, -7.379174546714949, -2.465453166998479,
    #      2.38728825563582], 0.07818918214340193)
    # >> ([-6.838227413566985, 5.172627776578082, 3.821491491433026, 0.2870244094391401, -0.6746436455147671,
    #      0.06884362084584597], 0.07818918214340193)
    # >> ([4.460576019684823, -5.99785694950206, 1.1461483263947543, 1.6718274482450295, 2.8119621601853972,
    #      4.558143853570719], 0.07818918214340193)
    # >> ([6.771209948382249, 4.56512818135654, 1.5761594758401503, -2.2447296028694113, 0.9443160179064141,
    #      -3.682253940642144], 0.07818918214340193)
    # >> ([-6.186849514657598, 5.06161993956806, 8.668965676037306, -3.7102835216912995, -1.4432818300688601,
    #      1.6564754802774728], 0.126990518739308)
    # >> ([-1.4292768024525109, -0.8003656789930753, -6.244325686025586, 3.479384554627577, -2.862038469646775,
    #      17.11167878562537], 0.22769013852017794)
    # >> ([11.170349199021425, 3.5593257150906124, 0.8816058176449565, 7.699938242742169, 3.5634783649068047,
    #      -24.255717928946794], 0.34550634375314965)
    #
    # B...
    # :: hit rate = 0
    # :: median = 0.03789142573934595
    # >> ([2.558065680535158, 1.0099654245211767, 0.7087949317907984, 1.0515456445434808, -0.8569483074895388,
    #      0.6318333777586815], 0.014753722436586403)
    # >> ([0.6266835339944521, -3.9932031599059883, -3.9442796441458015, 0.7588628572763767, 2.247644542524597,
    #      1.3293511053805815], 0.037297754724524845)
    # >> ([2.545572541033046, 5.071475539911049, 1.7754801633263355, 0.7241490956063714, -0.5137207573512796,
    #      -1.8399008941177826], 0.03731300345924071)
    # >> ([5.259635572259704, -0.04365446264957029, -1.9653091265245024, -2.1494637248343125, 1.7683276025426835,
    #      -0.5007507804635907], 0.037313538980726146)
    # >> ([-1.2326007998470274, 3.2088533443705316, -1.4800794029614508, -3.0306592133804386, 3.3508555177953383,
    #      -2.1989594454571915], 0.03735194372376943)
    # >> ([3.1566873343884225, -1.9724616873081615, 0.6985906117394478, 1.6704805710223027, -0.3821136389320543,
    #      -4.751565337927502], 0.03843090775492247)
    # >> ([-5.855729987969397, 0.3628017248161868, -0.5203011132722395, -0.8703951217628116, 1.6690500588655723,
    #      1.3316399248313502], 0.03936258685518623)
    # >> ([-3.2492414709288937, 0.6993535515563707, -5.278661383944218, 1.0430579390802066, 0.42269250044465423,
    #      0.33533589140696307], 0.04178966280034452)
    # >> ([-0.2983809940247468, 4.084898989152428, -3.341080351391007, -0.5956414201933953, -2.0942459555845048,
    #      -2.2804032709137303], 0.05109478713175086)
    # >> ([2.779032125011504, -6.781557455805519, 4.655101134825287, 2.7944816563041925, -2.115417535504122,
    #      0.8995298860215541], 0.07827319607202249)
    #
    # ** ** *f7 ** ** *
    # N = 3
    # FP...
    # :: hit rate = 8
    # :: median = 2.151746395552244e-60
    # >> ([-1.2054047608155037e-121, -4.0686825489393255e-121, -3.5511470832939347e-121], 7.438640671244466e-61)
    # >> ([6.267871437196818e-121, 7.536342643472823e-122, -4.01688980388185e-121], 8.650213043234792e-61)
    # >> ([9.067299908705762e-121, -1.0217605000816423e-120, -9.473028259662033e-121], 1.2893365639790047e-60)
    # >> ([-1.342500508341082e-120, 3.743581466691291e-122, -1.4542242137568671e-120], 1.4069521542795966e-60)
    # >> ([3.2523835360217207e-122, -2.7191541247937057e-120, 1.594140322059668e-120], 1.775433577096563e-60)
    # >> ([-2.4638591159590174e-120, 4.67344247291281e-120, 3.596426087244285e-120], 2.528059214007925e-60)
    # >> ([-6.1630315337938015e-118, 6.5339888718318505e-118, -3.8615908355088266e-118], 3.1268041210190643e-59)
    # >> ([1.3354749255796983e-117, 2.3801468055132053e-117, -2.650402484062166e-117], 6.167956917748149e-59)
    # >> ([1.5979159545033195e-05, -5.326413184016094e-06, -1.9987025245087796e-05], 0.005364481906723688)
    # >> ([-0.00021098902314690748, 5.224055190445779e-05, -3.95307066935289e-05], 0.015148820305729838)
    #
    # B...
    # :: hit rate = 0
    # :: median = 0.09498244583139573
    # >> ([-0.0026464474899512425, -0.002360345058605162, 0.0008821491633170808], 0.07790534479451217)
    # >> ([0.005650523019085085, 0.0048875832021622045, -0.00026226056206724024], 0.08687728862189403)
    # >> ([-0.0008344654247594008, -0.006270411620334926, -0.0035047547839894833], 0.08818618495109579)
    # >> ([-0.003314019829758763, -0.004076959646681644, -0.005698206757642765], 0.08843108329445608)
    # >> ([-0.004935266940719885, -0.002074242627259082, 0.004506113293700764], 0.09173096575619878)
    # >> ([-0.002169610104374442, 0.003170968614085723, 0.007271770130046207], 0.09823392590659266)
    # >> ([-0.003409387306874123, -0.004267694600912364, -0.006842616483027086], 0.11778425457470941)
    # >> ([0.001072884117547801, 0.005364420587739005, 0.006890300221584766], 0.11962783452915055)
    # >> ([0.005269053110623645, 0.015378005684851814, 0.001168251594663161], 0.12789967917062053)
    # >> ([-0.006747249005911726, -0.00998974322783397, -0.01037121313629541], 0.12894591798994542)
    #
    # N = 6
    # FP...
    # :: hit rate = 0
    # :: median = 2.3953836100316352
    # >> ([0.07495658016683993, -0.09867230402332976, -0.03132476700137939, -0.0827388210463564, 0.009854313113801512,
    #      0.036525535232336225], 0.3966084293340289)
    # >> ([0.10544052049049799, 0.0725116447406538, 0.2656665611045671, -0.1603571121127751, 0.24502834917481742,
    #      0.3207554744537807], 0.725134394289842)
    # >> ([0.33405857916865683, -0.10936306849445329, 1.325929042459442, 1.4934703910621057, -0.9052616365588877,
    #      0.9629175931895484], 1.5564783329723917)
    # >> ([-0.5762489044423823, -1.5107691972990274, -1.9487699555272413, 1.6993448732726077, 0.6988954627238041,
    #      -0.01458926552021768], 1.769508532629105)
    # >> ([-0.052759111381658244, 1.7882102704364504, -2.5717720980524352, 0.6687431803442543, 2.135554986602813,
    #      1.06162605526215], 1.9991340167062075)
    # >> ([-0.4548886542379208, 6.23943351314326, -4.613288091749675, 0.4164446614450653, 0.0680305851184054,
    #      0.26356979430404026], 2.7916332033570628)
    # >> ([2.0048240438544793, 1.4602198816637195, -1.3017202116189137, -8.985322495750902, 1.6258055490649719,
    #      0.20639921736926328], 3.0916451349719463)
    # >> ([-6.072237253384898, -0.03841070964312292, -5.82344395803326, 1.78792922187258, 2.2276603605786462,
    #      -3.5103703350599655], 3.0916451349719463)
    # >> ([7.424970361102164, 2.726446409304077, 2.766867670904756, -2.6019085981155765, -4.628614648206774,
    #      -6.0585659112617725], 3.4102113321934557)
    # >> ([7.30363068741175, 7.973961041702293, -4.930748472879044, -10.55137961147287, 10.976596162042167,
    #      5.456707901522216], 4.480875595433886)
    #
    # B...
    # :: hit rate = 0
    # :: median = 1.8092725195708668
    # >> ([0.9166960319023332, 0.21317015322215127, -0.14026171696745138, 0.300145292351381, 0.801587487024058,
    #      -0.5069496664760891], 1.1865285359979916)
    # >> ([0.18694409701542725, 1.136041229267704, -0.28922571622167226, 0.4892589994711898, -0.3075362718278285,
    #      -1.315570504937412], 1.3743712518592728)
    # >> ([1.9071826492226833, -1.2646442721578026, 0.1629114927823494, -0.02582074442899085, -0.5165817816647404,
    #      -0.5882027569783972], 1.5569941148216742)
    # >> ([-1.495242831822793, 1.548791670223082, -0.8467439874381952, -1.2139087743324168, -1.3872868477281841,
    #      -1.0002856255939605], 1.7705844171406628)
    # >> ([1.2453323580419422, 2.214838130396899, -0.8123163281995431, 1.0652785612480926, -0.4196884249155133,
    #      1.2521034489171328], 1.799962300675222)
    # >> ([-1.9377479256381633, -1.9885787909406574, 1.3533837096136665, -0.08304123069821401, 0.17015942104311677,
    #      0.6888631290736811], 1.8185827384665116)
    # >> ([-2.6366961654167937, 0.7141355105092586, 0.44920465908272433, 0.6638768500694567, -0.6577256477955089,
    #      1.2804275896203947], 1.874846497221445)
    # >> ([-2.1655808284668154, 1.6473062740832702, 0.6662610369973407, 0.9382490817304046, -0.07960800152206104,
    #      -1.2305980828276049], 1.9155769244302085)
    # >> ([-1.6979940881700983, -0.14407641605206578, -0.7964853269983934, -0.6563905031158939, -0.7659677343214639,
    #      -0.8716348989653113], 2.000661894968183)
    # >> ([-0.7050279164447417, -0.09105209877591136, 3.0286088126224584, 2.3509275202405533, -0.4578354157616715,
    #      -1.0164027292264635], 2.0813536035597098)
    #
    # HIT
    # RATE / MEDIAN - comparisons
    # ** ** *f6 ** ** *
    # N = 3 - -----
    # FP:
    # (1, 0.009715909877514362)
    # B:
    # (0, 0.00971591406479358)
    # N = 6 - -----
    # FP:
    # (0, 0.07818918214340193)
    # B:
    # (0, 0.03789142573934595)
    #
    # ** ** *f7 ** ** *
    # N = 3 - -----
    # FP:
    # (8, 2.151746395552244e-60)
    # B:
    # (0, 0.09498244583139573)
    # N = 6 - -----
    # FP:
    # (0, 2.3953836100316352)
    # B:
    # (0, 1.8092725195708668)


    ######### P_MUTATION = 0.5
    # ** ** *f6 ** ** *
    # N = 3 - -----
    # FP:
    # (0, 0.009715909877514362)
    # B:
    # (0, 0.035319751190180715)
    # N = 6 - -----
    # FP:
    # (0, 0.009715909877514362)
    # B:
    # (0, 0.3490223255742092)
    #
    # ** ** *f7 ** ** *
    # N = 3 - -----
    # FP:
    # (9, 1.6822968942031168e-29)
    # B:
    # (0, 2.0492047232471373)
    # N = 6 - -----
    # FP:
    # (0, 0.725134394289842)
    # B:
    # (0, 5.4137665471121)


def zad4_load_params():
    fp_solve_yn, b_solve_yn = False, False

    with open("zad4_params.txt", "r") as params_file:
        lines = params_file.readlines()
        lines = [l.split("=") for l in lines]
        for l in lines:
            if l[0].strip() == "MAX_NUM_EVAL":
                MAX_NUM_EVAL = int(float(l[1].strip()))
            elif l[0].strip() == "F_LIST":
                f_num_list = l[1].strip().split()
            elif l[0].strip() == "N_VAR":
                N_VAR = int(l[1].strip())
            elif l[0].strip() == "FP":
                fp_solve_yn = int(l[1]) == 1
            elif l[0].strip() == "B":
                b_solve_yn = int(l[1]) == 1
            elif l[0].strip() == "POP_SIZE_LIST":
                POP_SIZE_list = l[1].strip().split()
                POP_SIZE_list = [int(el) for el in POP_SIZE_list]
            elif l[0].strip() == "EXPLICIT_CONSTRAINT":
                EXPLICIT_CONSTRAINT = l[1].strip().split()
                EXPLICIT_CONSTRAINT = tuple([float(el) for el in EXPLICIT_CONSTRAINT])
            elif l[0].strip() == "P_MUTATION_LIST":
                P_MUTATION_list = l[1].strip().split()
                P_MUTATION_list = [float(el) for el in P_MUTATION_list]
            else: # N_DECIMAL
                N_DECIMAL = int(l[1].strip())

        return MAX_NUM_EVAL, f_num_list, N_VAR, fp_solve_yn, b_solve_yn, POP_SIZE_list, EXPLICIT_CONSTRAINT, P_MUTATION_list, N_DECIMAL


def zad4():
    print()
    print()
    print("************************* ZAD 4 *************************")

    MAX_NUM_EVAL, f_num_list, N_VAR, fp_solve_yn, b_solve_yn, POP_SIZE_list, EXPLICIT_CONSTRAINT, P_MUTATION_list, N_DECIMAL = zad4_load_params()


    f_goal_list = []
    if "f1" in f_num_list:
        f_goal_list.append(f1_goal)
    if "f3" in f_num_list:
        f_goal_list.append(f3_goal)
    if "f6" in f_num_list:
        f_goal_list.append(f6_goal)
    if "f7" in f_num_list:
        f_goal_list.append(f7_goal)

    ga_type_list = []
    if fp_solve_yn:
        ga_type_list.append("FP")
    if b_solve_yn:
        ga_type_list.append("B")


    f_goal_dict = dict(zip(f_goal_list, f_num_list))

    f_sol_list = []

    for fx_goal in f_goal_list:
        print()
        print("***** " + f_goal_dict[fx_goal] + " *****")
        f_sol_pop_size_list = []
        for POP_SIZE in POP_SIZE_list:
            print("POP_SIZE = ", POP_SIZE)

            f_sol_mutation_list = []

            for P_MUTATION in P_MUTATION_list:
                print("-- P_MUTATION =",P_MUTATION)
                fx_list = []

                if fp_solve_yn:
                    print("FP...")
                    fp_list = []
                    for i in range(GA_REPEAT_TIMES):
                        if print_yn:
                            print()
                            print("%5d run:" % (i + 1))

                        sol = run3TournamentEliminationSelection(sol_form="FP", max_f_eval=MAX_NUM_EVAL, f_goal=fx_goal, pop_size=POP_SIZE,
                                                                 explicit_constraint=EXPLICIT_CONSTRAINT, n_var=N_VAR,
                                                                 pm=P_MUTATION)
                        print(i, "SOL =", sol)
                        fx_goal.reset_called_times()
                        fp_list.append(sol)

                    fp_list.sort(key=itemgetter(1))
                    hit_rate, median = get_hit_rate_and_median(fp_list)
                    print(":: hit rate =", hit_rate)
                    print(":: median =", median)
                    fx_list.append((hit_rate, median))
                    for sol in fp_list:
                        print(">>", sol)
                    print()

                if b_solve_yn:
                    print("B...")
                    b_list = []
                    for i in range(GA_REPEAT_TIMES):
                        if print_yn:
                            print()
                            print("%5d run:" % (i + 1))

                        sol = run3TournamentEliminationSelection(sol_form="B", max_f_eval=MAX_NUM_EVAL, f_goal=fx_goal, pop_size=POP_SIZE,
                                                                 explicit_constraint=EXPLICIT_CONSTRAINT, n_var=N_VAR,
                                                                 pm=P_MUTATION, num_decimal=N_DECIMAL)
                        print(i, "SOL =", sol)
                        fx_goal.reset_called_times()
                        b_list.append(sol)
                    b_list.sort(key=itemgetter(1))
                    hit_rate, median = get_hit_rate_and_median(b_list)
                    print(":: hit rate =", hit_rate)
                    print(":: median =", median)
                    fx_list.append((hit_rate, median))
                    for sol in b_list:
                        print(">>", sol)
                    print()

                f_sol_mutation_list.append(fx_list)

            f_sol_pop_size_list.append(f_sol_mutation_list)

        f_sol_list.append(f_sol_pop_size_list)

    print("")
    print("HIT RATE / MEDIAN - comparisons")
    for i in range(len(f_num_list)):
        print("***** " + f_num_list[i] + " *****")
        f_pop_size_vals_list = f_sol_list[i]
        for k in range(len(f_pop_size_vals_list)):
            print()
            print("POP_SIZE =", POP_SIZE_list[k],"------")
            f_mutation_vals_list = f_pop_size_vals_list[k]
            for m in range(len(f_mutation_vals_list)):
                print("--- P_MUTATION =", P_MUTATION_list[m])
                fx_vals = f_mutation_vals_list[m]
                for j in range(len(ga_type_list)):
                    print("  " + ga_type_list[j] + ":")
                    print(fx_vals[j])
        print()

    # boxplots made using: https://datatab.net/charts/boxplot
    # best params: POP_SIZE=200, FP, P_MUTATION=0.6 -> median = 0.009715909877514362
    # for B - best params: POP_SIZE=30, P_MUTATION=0.1 -> median = 0.009715909947296097
    #
    # ** ** *f6 ** ** *
    # POP_SIZE = 30
    # -- P_MUTATION = 0.1
    # FP...
    # :: hit rate = 0
    # :: median = 0.02346999249250109
    # >> ([0.09724100270870253, 1.4504201174670237, 2.781530630654207], 0.009715909877514362)
    # >> ([-2.956694544092034, -0.8373937514415775, 0.6378213369058447], 0.009715909877514362)
    # >> ([-1.4357818037277852, 1.9115373141571832, 2.033382079676444], 0.009715909877514362)
    # >> ([1.8911312975046388, -2.5042732736233897, -0.048215735328370944], 0.009715909877514362)
    # >> ([2.2019342738137877, 1.3236864673772668, -1.8026165890359118], 0.009715909877514362)
    # >> ([0.15243774240404737, -4.997030011050211, 3.7959216169469636], 0.03722407510748782)
    # >> ([3.6429189143909695, -3.4647817233391134, 3.758589695772672], 0.03722407510748782)
    # >> ([3.46006895805321, 5.265140147268097, -0.5029546295280092], 0.038950646612933404)
    # >> ([3.484520322149142, -8.679216535807385, 1.0921604603245725], 0.07818918214340193)
    # >> ([-3.2677715603304174, 3.2536825946078016, 8.209672017258523], 0.07818918214340193)
    #
    # B...
    # :: hit rate = 0
    # :: median = 0.009715909947296097
    # >> ([-2.179027642740081, 0.3618480500450332, -2.229572405611229], 0.009715909877579532)
    # >> ([-0.7518533477083906, 2.0022640239067186, -2.2969018444546876], 0.009715909883722229)
    # >> ([-2.415252883554878, -1.8787154573037412, 0.6979230393996403], 0.009715909896843511)
    # >> ([-0.6809953122116639, -0.9676699484205002, -2.906872228084673], 0.009715909919557397)
    # >> ([-2.69153246475814, -1.6118772563349069, -0.08685592978283552], 0.009715909934255973)
    # >> ([0.8447889541573304, 2.791143794605155, 1.1601692009779043], 0.009715909960336222)
    # >> ([1.9149074148690275, -1.3405567839416435, 2.0942936393230625], 0.009715909961983793)
    # >> ([-1.5451200223541335, 1.8788585085194072, -1.9830474772679665], 0.009715910149564855)
    # >> ([2.724958765487088, -1.5193708035329792, -0.3407241538639809], 0.009715910158035468)
    # >> ([2.64561302452708, -1.015735156886656, -1.348663019496449], 0.009715910964172514)
    #
    # -- P_MUTATION = 0.3
    # FP...
    # :: hit rate = 0
    # :: median = 0.009715909877514362
    # >> ([2.008526161729405, 0.8249624500621747, -2.2661303109724145], 0.009715909877514362)
    # >> ([2.369971069243894, 1.299071003038534, -1.5955370997942533], 0.009715909877514362)
    # >> ([-0.07260166151454955, -1.4647007404706787, -2.7747914738011445], 0.009715909877514362)
    # >> ([3.09581846616574, -0.1019593198833276, -0.5055683049134225], 0.009715909877514362)
    # >> ([-1.3868267067671116, 2.204261909242435, 1.7515787454082286], 0.009715909877514362)
    # >> ([0.10884280199206645, 2.5959265907064646, -1.7605128099973997], 0.009715909877514362)
    # >> ([2.791625769105135, -1.2888755321887215, -0.6290567583547472], 0.009715909877514362)
    # >> ([-1.1532626954047982, -2.753705610887499, -0.9680793003339783], 0.009715909877514362)
    # >> ([-3.028127913199313, -0.7903748236399047, 0.23629632427214975], 0.009715909877514362)
    # >> ([2.567975635851666, 1.5227577365802671, 0.9678827393619607], 0.009715909877514362)
    #
    # B...
    # :: hit rate = 0
    # :: median = 0.011591551175977516
    # >> ([-1.8928298439168216, -1.5216596229837478, -2.0044097921418143], 0.009823479475013774)
    # >> ([-2.646042178174099, -1.6543157836512492, 0.07622245608447287], 0.009997233227885316)
    # >> ([0.154328419841967, 0.7095578716077213, 3.0739083642522615], 0.010109508942027778)
    # >> ([-1.6237028234972115, -2.1240006084445042, -1.5982397071074033], 0.010265193142141305)
    # >> ([2.106309941439598, 0.1361132317129332, 2.270914206940752], 0.0111437191268009)
    # >> ([2.096773193728062, -0.8310083537141608, -2.2518883952562305], 0.012039383225154132)
    # >> ([2.030588164609995, -1.6945608589939383, 1.7977007854942286], 0.013174963268439421)
    # >> ([-0.11012559419899048, -2.9398693751665945, 0.7631543937465679], 0.019347617340473355)
    # >> ([-0.28092874571263593, 2.3549329542793984, -2.265525944483734], 0.029170336273147246)
    # >> ([4.432990280623571, 3.142572947775335, 3.1371370015797595], 0.037230782545091656)
    #
    # -- P_MUTATION = 0.6
    # FP...
    # :: hit rate = 0
    # :: median = 0.009715909877514362
    # >> ([2.8269737053229806, 0.4968872950008818, 1.2694131127555153], 0.009715909877514362)
    # >> ([-0.5151492305919962, -2.9549432395407136, -0.9235901126061389], 0.009715909877514362)
    # >> ([1.8976010687203368, 1.5159783541627774, 1.987713960393914], 0.009715909877514362)
    # >> ([-0.5042111241641745, -2.4271880326132558, -1.924738003719932], 0.009715909877514362)
    # >> ([-1.479520145612442, 0.2257253991221781, 2.758650966203562], 0.009715909877514362)
    # >> ([1.9849492804675009, -1.8615971299273357, 1.5634959115159595], 0.009715909877514362)
    # >> ([-0.5868332733357733, -2.960780843884061, 0.8599363236866571], 0.009715909877514362)
    # >> ([0.6011946716425667, -1.0552838116318326, 2.8939640669168396], 0.009715909877514362)
    # >> ([2.7628852703722684, -1.0867984075667143, -1.0175564790449978], 0.009715909877514362)
    # >> ([1.5708306448625624, 1.935886525306827, -1.9065469863136832], 0.009715909877514362)
    #
    # B...
    # :: hit rate = 0
    # :: median = 0.038481913326057715
    # >> ([0.28040422458850145, -0.9945635769670389, 3.009726052153617], 0.011587615697019216)
    # >> ([-1.4569051060224112, -1.9340285940306643, -2.0783195869062325], 0.012418741114863963)
    # >> ([-3.651501489401575, -3.6063926727260025, -3.6260383730117667], 0.037266614358708317)
    # >> ([-4.550149226259812, 4.221751318812991, -0.7770303616668528], 0.037659772887144505)
    # >> ([5.147006581786428, 0.6054165865977197, -3.4790770907769684], 0.038370726001925926)
    # >> ([3.127790988822454, 4.872729717602596, -2.521969090446987], 0.038593100650189505)
    # >> ([-3.7872094093367608, 4.582621852217606, -1.8194922540150955], 0.04054960010982822)
    # >> ([-1.380134286944532, -2.7840389175600606, 5.545261166220271], 0.043044012481593896)
    # >> ([0.32579914369542706, -4.662492114301735, 4.345729039062995], 0.0473620836647628)
    # >> ([1.5962846738265455, 5.490710969310264, 2.2650014233595925], 0.05208039315703106)
    #
    # -- P_MUTATION = 0.9
    # FP...
    # :: hit rate = 0
    # :: median = 0.009715909877514417
    # >> ([2.1869111894814424, 0.8778003730646119, 2.0729141199483827], 0.009715909877514362)
    # >> ([-1.0419129488649015, -0.4872594131968713, -2.9201168894628475], 0.009715909877514362)
    # >> ([-1.614364747929728, 2.5834389436085523, 0.7548222609978097], 0.009715909877514362)
    # >> ([0.7332169217449854, 0.5089571959577308, -3.008893893440977], 0.009715909877514417)
    # >> ([0.4105419332534569, 0.22749537603178127, -3.1031899912867047], 0.009715909877514417)
    # >> ([-1.6697493840772677, 2.525707118511804, 0.8263337564938665], 0.009715909877514417)
    # >> ([2.5008348775067932, 0.9562291193373652, -1.6375401491074628], 0.009715909877514417)
    # >> ([-2.6843564943632847, 1.6160533233946262, 0.18080050163393097], 0.009715909877514417)
    # >> ([-1.795085099807384, -1.9582325577641764, 1.6712513843727663], 0.009715909877521134)
    # >> ([-0.485370462347226, -1.3106855707972835, 2.810090976257741], 0.009715909878260043)
    #
    # B...
    # :: hit rate = 0
    # :: median = 0.01219755298020539
    # >> ([-2.58195523355257, 0.2537966984733089, 1.7683276025426835], 0.009717425370772581)
    # >> ([1.3076073205982794, -0.4290344376728257, 2.8105987599367026], 0.009796275846609859)
    # >> ([0.4832508484129221, -2.0058403042985447, 2.38602275181902], 0.00996344867470178)
    # >> ([2.297903202964399, -0.6001236916178243, -2.089763684160083], 0.010329001258697956)
    # >> ([-1.4732129446091378, -2.8016819008264093, -0.2748252271772529], 0.01119437756310726)
    # >> ([2.618910130934779, -0.16715534551398292, 1.8279322757397978], 0.013200728397303518)
    # >> ([-1.7589339060468205, 1.599622535525583, 2.209783654109785], 0.020984771434740535)
    # >> ([1.431108203462692, 5.585506241562953, 2.485491030450362], 0.03722667891968867)
    # >> ([2.533937708824972, 5.7694701049185255, -1.1683231202712676], 0.053154102863737396)
    # >> ([8.627251924158067, 3.6266582616130165, 0.7664922554456055], 0.0787718064582012)
    #
    # POP_SIZE = 50
    # -- P_MUTATION = 0.1
    # FP...
    # :: hit rate = 0
    # :: median = 0.03722407510748782
    # >> ([0.9739795039026606, -2.5176249218300604, 1.6009421120025855], 0.009715909877514362)
    # >> ([2.0901041600522743, -0.32354770179596426, 2.3188075486926323], 0.009715909877514362)
    # >> ([-0.4944023825430293, 0.3180108554229613, 3.0829405364466935], 0.009715909877514362)
    # >> ([1.919460602321563, 1.6902127242654537, -1.819048902705078], 0.009715909877514362)
    # >> ([1.2566590315661672, 5.961202987029708, -1.512426573577668], 0.03722407510748782)
    # >> ([0.7498963149836608, 5.763145407026779, -2.3719986527518606], 0.03722407510748782)
    # >> ([5.94385107751606, 1.796463453896112, 0.9197393711231505], 0.03722407510748782)
    # >> ([5.1995711050840265, 2.981541288918456, -1.8647892587585213], 0.03722407510748782)
    # >> ([-3.207522292240884, -5.388362658243139, 0.2826936482178225], 0.03722407510748782)
    # >> ([7.0083902185790485, -3.6097093011671144, -5.149365140756125], 0.07818918214340193)
    #
    # B...
    # :: hit rate = 0
    # :: median = 0.009715910085578316
    # >> ([2.3841869278840022e-05, 0.0007867816862017207, 0.000977516640432441], 1.5767069395100641e-06)
    # >> ([-3.078152216983895, 0.1378298463010097, 0.5967381461802219], 0.009715909896460373)
    # >> ([-3.090263886577553, -0.06587508481744919, -0.5440476150739713], 0.009715909898109776)
    # >> ([0.4672291122575416, 2.9185547440313115, -1.0554080273666528], 0.009715909979447268)
    # >> ([0.40256996277329904, 2.7687324374830453, 1.4220482931367329], 0.009715910022386365)
    # >> ([1.8643626519978724, 0.1925707781652335, -2.51739145154545], 0.009715910148770268)
    # >> ([1.4456994274613564, 0.3985645287344539, -2.7570499215364066], 0.009715910350655221)
    # >> ([-0.1423598014639893, -1.8089064640552834, 2.5608313373715035], 0.009715910676366568)
    # >> ([-1.7880209865670125, 0.3788234609715744, -2.5513422733985323], 0.009715911011139222)
    # >> ([0.8041624089061727, 2.940870733676306, -0.7451776243103154], 0.009715918862707285)
    #
    # -- P_MUTATION = 0.3
    # FP...
    # :: hit rate = 0
    # :: median = 0.009715909877514362
    # >> ([0.6386068559516331, 0.14140601442837422, -3.069572054446672], 0.009715909877514362)
    # >> ([-0.35356863356477275, 3.057226667017484, -0.6151758377129745], 0.009715909877514362)
    # >> ([-2.787408398938539, 1.4035305612352007, 0.3324806200760116], 0.009715909877514362)
    # >> ([-0.5536038437205695, 1.9721426461330862, 2.3778694587318685], 0.009715909877514362)
    # >> ([0.8802536618187916, 1.1947839670868339, -2.765453261197883], 0.009715909877514362)
    # >> ([-2.281405398901219, 0.16659279684182868, 2.1488422976151704], 0.009715909877514362)
    # >> ([3.1193771187609682, 0.18121061271734779, -0.29450986768754617], 0.009715909877514362)
    # >> ([1.1938217764944312, 2.008244258694739, -2.0956697111587954], 0.009715909877514362)
    # >> ([1.1070237732484254, -2.763464364259638, -0.9939064531773346], 0.009715909877514362)
    # >> ([-2.310426923908495, -2.0715724498417774, 0.4696828641660164], 0.009715909877514362)
    #
    # B...
    # :: hit rate = 0
    # :: median = 0.010472343526134104
    # >> ([-1.428294862887796, -0.21445761416321574, 2.786661523180733], 0.0097159596662445)
    # >> ([-2.723766672023146, 0.4556896475265688, 1.4890916295488452], 0.00971684788393562)
    # >> ([-1.325393355080294, 1.9523868333753711, 2.0527134193007583], 0.009831417142682153)
    # >> ([2.6105177929486203, 1.7210253338934578, -0.38774032008186055], 0.009862968961571017)
    # >> ([-0.1112700039243748, 3.005243780729195, -0.9621386347478023], 0.010069395821022231)
    # >> ([1.03600074577367, -1.551318908366639, -2.4810564427644977], 0.010875291231245976)
    # >> ([2.4658453301645977, -0.7854226996530045, -1.6686209052185532], 0.013161433129147804)
    # >> ([1.7118700560903832, 1.31437841147347, 2.3972761141186325], 0.017154627060952443)
    # >> ([-4.162575799262903, -1.0494952437855005, 4.568221363173187], 0.037289528514277914)
    # >> ([4.2992850777078075, -4.283692495199439, -1.4700658178643309], 0.0382059643282332)
    #
    # -- P_MUTATION = 0.6
    # FP...
    # :: hit rate = 0
    # :: median = 0.009715909877514362
    # >> ([-0.18373155157403293, 1.5407655215426042, 2.728070988062306], 0.009715909877514362)
    # >> ([0.5231562633790504, -2.7398979953056317, -1.4385247582954959], 0.009715909877514362)
    # >> ([3.0148947962397834, 0.21164900844107964, -0.8459911602082636], 0.009715909877514362)
    # >> ([-1.9592330016966635, -0.7753765126495692, 2.3260017781664555], 0.009715909877514362)
    # >> ([-3.034197985855788, -0.2960281586696372, 0.7457190409269528], 0.009715909877514362)
    # >> ([2.6328443932926238, -1.2479701020355796, -1.166528184987603], 0.009715909877514362)
    # >> ([1.818749960364511, -2.555520078792194, -0.10748342377871642], 0.009715909877514362)
    # >> ([2.5375082492215313, 1.2422458149103541, 1.366734865699979], 0.009715909877514362)
    # >> ([-2.3477259912669526, -1.9816025865020115, 0.6414988990607896], 0.009715909877514362)
    # >> ([-2.2218827879111283, 1.756791320833201, -1.351668637622817], 0.009715909877514362)
    #
    # B...
    # :: hit rate = 0
    # :: median = 0.044814214987516976
    # >> ([1.4316804083253842, -2.133251253724694, -1.7399557781008568], 0.01095949310698452)
    # >> ([-5.655362918549976, -0.8774523150693483, 2.5333655039622798], 0.037540305615566705)
    # >> ([-5.424859726362094, -2.8368724998819843, 1.2535339610738632], 0.03796534672411839)
    # >> ([3.6658542947074366, -4.74708306650308, 2.0694027277959464], 0.04144600745141708)
    # >> ([5.078055895832009, -2.8824581539431335, 2.1126041949292116], 0.041457677454562525)
    # >> ([-5.020692358347112, -3.5812156587675403, 0.12581354418447432], 0.04817075252047143)
    # >> ([-0.45917056044128657, 6.029632582489285, -2.0729790081877724], 0.04948318954768699)
    # >> ([5.833461682062946, 0.4883053247000362, 1.6391046710513493], 0.0732353180071274)
    # >> ([-6.218221768484959, 1.7880686703055773, 0.43270608554176704], 0.0764155351087783)
    # >> ([6.080463447791793, -7.1508203271962785, -0.1334906260922537], 0.0788844225523409)
    #
    # -- P_MUTATION = 0.9
    # FP...
    # :: hit rate = 0
    # :: median = 0.009715909877514362
    # >> ([1.1668410947211558, -1.1339587438987468, 2.68378583811597], 0.009715909877514362)
    # >> ([1.900251652216207, 1.7082566372686705, 1.8223583342605643], 0.009715909877514362)
    # >> ([2.450418674288759, -0.6348145612790452, -1.8554098680064524], 0.009715909877514362)
    # >> ([-1.4291438341581972, -2.0507606237358136, 1.8978977147723066], 0.009715909877514362)
    # >> ([-2.9135342361168157, -0.43085695018369297, -1.0843281416809174], 0.009715909877514362)
    # >> ([2.6908651718978707, 1.239412560169765, 1.0359479199279815], 0.009715909877514362)
    # >> ([-1.117258437042751, -2.7854781436299074, -0.9181133218461843], 0.009715909877514362)
    # >> ([-1.3821964074502802, -2.652132560384882, -0.9517420622938159], 0.009715909877514362)
    # >> ([-3.1063019023989407, -0.4183230516067087, 0.1611871528664868], 0.009715909877514362)
    # >> ([-1.06498500300142, 2.743845122592998, -1.0895906780664235], 0.009715909877514417)
    #
    # B...
    # :: hit rate = 0
    # :: median = 0.037371484403063954
    # >> ([-2.558971671567761, -1.796317957076056, -0.5901101065207044], 0.011548672640778301)
    # >> ([2.7266753800751644, 0.9441618653115569, -1.0825877583445305], 0.012855207208351094)
    # >> ([-1.2508159879760683, -0.11937623947918041, -3.0208363632375566], 0.02702133114070765)
    # >> ([-2.519871005950449, 4.005267145761081, 4.128386558717047], 0.037230444100817606)
    # >> ([2.792765041716116, 0.12161737519138427, -5.607297710083827], 0.03735041105485909)
    # >> ([-4.597070025000583, -1.870990691657397, -3.8209694962356124], 0.03739255775126882)
    # >> ([0.8761648541282909, -5.170800767326725, -3.421856604507731], 0.03743375185586939)
    # >> ([4.527213348013568, 0.15442378731908235, -4.293229242910982], 0.03842812892406977)
    # >> ([-5.087735694759218, -1.0942225905526115, -3.7103432227817663], 0.04922787785039484)
    # >> ([2.08571056638268, -1.2826687253326057, 1.5883691732259706], 0.056486397143234335)
    #
    # POP_SIZE = 100
    # -- P_MUTATION = 0.1
    # FP...
    # :: hit rate = 0
    # :: median = 0.009715909877514362
    # >> ([-2.551184902726081, -1.6456218197817867, 0.7959091598817396], 0.009715909877514362)
    # >> ([-1.1538954893084972, 0.29492029322505803, 2.903727637289631], 0.009715909877514362)
    # >> ([-0.07413945288624468, -2.450463814741296, 1.9595452065809846], 0.009715909877514362)
    # >> ([1.4781206573921317, -0.299621923646031, -2.7523577156038375], 0.009715909877514362)
    # >> ([1.3000544345755616, 1.836032692095818, 2.188362266579526], 0.009715909877514362)
    # >> ([-1.4040373457693596, -1.7747952378017637, -2.1745960468848198], 0.009715909877514362)
    # >> ([-3.03061626882932, -0.4005886840441367, 0.7106199487424124], 0.009715909877514362)
    # >> ([-2.5913182179449583, -1.7571421311093673, 0.2181934923098327], 0.009715909877514362)
    # >> ([-1.3592420568221772, -1.518539822310193, -2.457489620413937], 0.012587290304862275)
    # >> ([-5.1930022689162305, -0.31055893455469796, 3.512669593549224], 0.03722407510748782)
    #
    # B...
    # :: hit rate = 0
    # :: median = 0.00971591028760338
    # >> ([-2.8321041260262163, 0.5788090604825342, -1.2223964797956839], 0.009715909877514861)
    # >> ([-2.931286302226212, 0.09567742141600633, 1.1173492037530934], 0.00971590989207749)
    # >> ([1.5320069942507644, -2.268386968797195, -1.5353925396883668], 0.009715909902792308)
    # >> ([2.3996603010465165, 1.4488465542061633, 1.4115578706540433], 0.009715909984249094)
    # >> ([-0.9756808164981905, -2.9827847398685137, -0.03545285961764222], 0.009715910284338158)
    # >> ([-0.8611444764826146, -3.016926296675827, 0.08089546246312551], 0.009715910290868601)
    # >> ([3.107859186105344, 0.4093410536484896, 0.1535654800250441], 0.009715910482233858)
    # >> ([-2.0098457383373898, -1.8446692679735506, 1.5516526945365428], 0.009715910651761361)
    # >> ([0.17206677058543107, -3.116966780169861, -0.323558007983209], 0.009715912245583824)
    # >> ([1.9669780573740212, -1.2363201314545336, 2.110029273047097], 0.009715913349138405)
    #
    # -- P_MUTATION = 0.3
    # FP...
    # :: hit rate = 1
    # :: median = 0.009715909877514362
    # >> ([-1.1419408789861783e-07, 8.790630400723415e-07, -7.92852295563452e-07], 1.4157564010019996e-12)
    # >> ([2.4893874013427455, 1.7812937932151978, -0.6928418083981168], 0.009715909877514362)
    # >> ([-0.6114804797877422, -2.508063533742067, 1.7848798050374328], 0.009715909877514362)
    # >> ([-0.6385705182217452, -3.072770997790138, -0.019825799093256886], 0.009715909877514362)
    # >> ([-0.5329751549279903, -2.5969873303361473, -1.6797860760946763], 0.009715909877514362)
    # >> ([-2.858724045057064, 0.09061808016295089, -1.2921192589660477], 0.009715909877514362)
    # >> ([-1.2619385309369622, -0.10673778773292734, 2.8716206505423756], 0.009715909877514362)
    # >> ([-2.3204242110835347, 2.1099458356806737, -0.11767331950234755], 0.009715909877514362)
    # >> ([0.5251548930057566, 2.5150259751169948, -1.8024826388970752], 0.009715909877514362)
    # >> ([-1.385596264728655, -0.05431936750710125, 2.815538911364679], 0.009715909877514362)
    #
    # B...
    # :: hit rate = 0
    # :: median = 0.01515809998931969
    # >> ([-2.5253069521460247, -0.5560639171905137, 1.7801531697049882], 0.009716593886524683)
    # >> ([-2.9425396645258317, -0.8277658594922386, -0.7768396267126221], 0.009949733663117077)
    # >> ([-2.402855111529881, -0.9969477638949229, -1.8250235676877864], 0.011230786866490872)
    # >> ([1.3284927980865433, -2.548958086470641, -1.131129804196263], 0.01212457795282229)
    # >> ([3.1797662638503397, -0.2733947150205225, 0.02176762665158094], 0.012478361821736894)
    # >> ([-2.7821315680177534, 1.5157945231411531, 0.6264927990402214], 0.017837838156902486)
    # >> ([-0.635791128058969, 2.4629843058511227, -2.004886629527391], 0.01956009441733031)
    # >> ([-1.8354186226933606, -1.2139087743324168, 2.385450546956328], 0.02088603516222881)
    # >> ([3.7128704609253234, -4.143883773748293, 2.9473557221201503], 0.03756577921111265)
    # >> ([3.7295597694205114, -1.9603500177145037, -4.7185681908455805], 0.039426129023308165)
    #
    # -- P_MUTATION = 0.6
    # FP...
    # :: hit rate = 0
    # :: median = 0.009715909877514362
    # >> ([2.7091960795144585, -0.7591952429864645, -1.3906711125423077], 0.009715909877514362)
    # >> ([-1.4555257731100677, -0.5990174522122279, 2.715273428626624], 0.009715909877514362)
    # >> ([0.14434143866956706, -2.346366638055627, -2.0793787823161023], 0.009715909877514362)
    # >> ([-1.3089436607878362, 0.7271153487738069, 2.758270604972069], 0.009715909877514362)
    # >> ([3.0356600791405803, 0.06272163064890683, -0.7943052565200606], 0.009715909877514362)
    # >> ([-0.76598389229677, 1.3558337299100587, -2.7248982631889076], 0.009715909877514362)
    # >> ([-1.744430491346574, -0.027581227107277877, 2.6088864502288427], 0.009715909877514362)
    # >> ([-2.3462227389473007, -1.863978077846898, 0.9332264223756254], 0.009715909877514362)
    # >> ([-2.5587690642823686, 1.816956594009313, -0.0381654731396362], 0.009715909877514362)
    # >> ([3.033764103519724, -0.6848532683660371, -0.4211155853064774], 0.009715909877514362)
    #
    # B...
    # :: hit rate = 0
    # :: median = 0.03147139654201725
    # >> ([-1.0605578711308823, 0.15203960039119835, 2.997805117514183], 0.011703678712265686)
    # >> ([1.9390353865792207, 2.2319089088005626, 0.859761648064449], 0.013182440978563748)
    # >> ([-2.688385338013333, 0.5788090604825342, 1.6938456029155802], 0.017869952664681554)
    # >> ([-1.6882666055043245, -2.7324451124406437, -0.39899368238147304], 0.019130338732811503)
    # >> ([1.8778094662711382, -0.2969504818680164, 2.3170720658645862], 0.029202989008407787)
    # >> ([-1.4258153084827896, 2.4044286749022845, 1.0368590530677082], 0.03373980407562671)
    # >> ([3.5121219215974406, 5.181148138593741, -0.4521133671347499], 0.037226167559335055)
    # >> ([-2.4903070880446876, 5.208613972002972, -2.4336588066381495], 0.03735411789515081)
    # >> ([0.5849125790179173, 2.0421276293409534, -2.0547638200587386], 0.04229496421030399)
    # >> ([-1.7492064233810538, 5.576255596282763, 1.9863376552284535], 0.04734801406277728)
    #
    # -- P_MUTATION = 0.9
    # FP...
    # :: hit rate = 0
    # :: median = 0.00971590987751439
    # >> ([1.018949612746078, -0.25311432659442973, 2.957661543463928], 0.009715909877514362)
    # >> ([-0.9576854513835551, 2.9834867440372053, 0.17813587213541648], 0.009715909877514362)
    # >> ([3.020631067287036, 0.7407061269108344, -0.42098617699246016], 0.009715909877514362)
    # >> ([-1.354090934662255, -0.4628245300692356, -2.7932629992515703], 0.009715909877514362)
    # >> ([-0.6509184873817294, 3.0350619179341987, 0.46345578222972394], 0.009715909877514362)
    # >> ([-0.7685523172142356, -2.897818444449939, 0.9284732617352128], 0.009715909877514417)
    # >> ([-2.9800079139976914, -0.46476555843735523, 0.8681202462369184], 0.009715909877514417)
    # >> ([2.7075412458878514, -1.3860030403272101, 0.7735004695543988], 0.009715909877514417)
    # >> ([-1.3131951363900563, 2.4483153919816534, -1.459916864705574], 0.009715909877514417)
    # >> ([0.7516145260933276, 1.984521271408901, -2.3123230552618392], 0.009715909877514417)
    #
    # B...
    # :: hit rate = 0
    # :: median = 0.017391468556801903
    # >> ([-1.7994650838208628, -2.4003755571248817, 1.0218863591605967], 0.010644263351412941)
    # >> ([0.4287960189800373, 2.666021664629781, 1.6704805710223027], 0.011039731148851595)
    # >> ([0.18713483196965797, 2.2234212033372955, 2.266431935516323], 0.011443830103658181)
    # >> ([2.3935567825111335, 1.3134247367023164, 1.4573342596694303], 0.011585312095949984)
    # >> ([-0.7043603441049342, -1.4765508063081754, 2.7682556000974685], 0.015525515488640063)
    # >> ([-1.1109118990478066, -1.997447966312393, -2.2926103079844964], 0.019257421624963744)
    # >> ([2.277017725476135, 1.0980849733757765, 2.037836092870762], 0.02123037592469751)
    # >> ([-2.1373520552406617, -5.449464535457864, -2.1730194916818135], 0.038245064421955965)
    # >> ([-4.055859592370794, -0.9731058946160758, -4.6195767495998155], 0.039846773660405344)
    # >> ([1.9942531558290284, -4.537942189189046, -3.7626046002409907], 0.0399286465235994)
    #
    # POP_SIZE = 200
    # -- P_MUTATION = 0.1
    # FP...
    # :: hit rate = 0
    # :: median = 0.009715909877514362
    # >> ([1.0253825640677925, 2.8046128175909795, 0.9658284029516366], 0.009715909877514362)
    # >> ([-0.6474154193092099, -0.2255174364091971, 3.0626919754154174], 0.009715909877514362)
    # >> ([-1.7106528931657337, -0.06496063858473715, -2.6305006686575587], 0.009715909877514362)
    # >> ([-2.170308268885137, -2.1434295593994417, -0.738619467624643], 0.009715909877514362)
    # >> ([0.7815111065482414, -1.9087620677570156, 2.365577042959682], 0.009715909877514362)
    # >> ([-1.2767817750466672, 2.3286460662036785, 1.672519887799841], 0.009715909877514362)
    # >> ([2.910566542962914, 1.1080881215671756, 0.38836851241057785], 0.009715909877514362)
    # >> ([1.4647444893964359, -1.3327374482802723, 2.434834994131095], 0.009715909877514362)
    # >> ([-2.570032911946079, 1.7973529197110838, 0.12058305513138162], 0.009715909877514362)
    # >> ([0.4966225737952358, 1.1332485176818905, 2.884302478705642], 0.009715909877514362)
    #
    # B...
    # :: hit rate = 0
    # :: median = 0.009715910950772039
    # >> ([3.074862039023415, -0.5775215995414769, 0.24855148723196407], 0.009715909877561657)
    # >> ([0.18303403045369748, 2.967096789883037, 1.0064368278679083], 0.009715909878759865)
    # >> ([-0.1693487974876362, 2.936960667114576, -1.0934596507356886], 0.009715909883885987)
    # >> ([-1.3724095212981808, 2.7955306985524615, 0.3892185159771486], 0.009715910107785997)
    # >> ([0.12295251987101352, 1.6920336208503883, -2.640415497024293], 0.00971591085303003)
    # >> ([-2.3641359158210378, -2.063632995430467, 0.046372435747358054], 0.009715911048514048)
    # >> ([-3.115154798104669, -0.3448249553799414, 0.16510494475600268], 0.009715911644614383)
    # >> ([0.7270101199198322, -3.023888122505248, 0.4210712533336931], 0.009715912806491867)
    # >> ([2.6459944944355414, -0.19242772694956045, 1.6769655594661472], 0.009715914740141551)
    # >> ([-1.3406521514187588, 2.3416768749603634, -1.603198815917409], 0.009715932751829914)
    #
    # -- P_MUTATION = 0.3
    # FP...
    # :: hit rate = 0
    # :: median = 0.009715909877514362
    # >> ([2.4874020680484894, -0.48725557073448944, 1.850810614850626], 0.009715909877514362)
    # >> ([2.892170861445673, 0.008535311304201766, -1.2187542063871162], 0.009715909877514362)
    # >> ([1.983442065907502, 0.3663831982141621, 2.404539020468074], 0.009715909877514362)
    # >> ([1.24646240590193, 2.687129046572801, 1.0371865477693232], 0.009715909877514362)
    # >> ([-0.3912460596619527, 2.367181422415266, -2.0232314770310396], 0.009715909877514362)
    # >> ([-1.9881622462222035, -1.4544455625157964, 1.9447071650798302], 0.009715909877514362)
    # >> ([-2.261653876359071, -1.978662133173761, -0.9054859904733827], 0.009715909877514362)
    # >> ([-2.7984713356721374, -1.1821132316763827, -0.7881963360934581], 0.009715909877514362)
    # >> ([2.2199551190324933, -1.095805493068458, -1.9290144004155843], 0.009715909877514362)
    # >> ([-0.3251873055028015, -0.5197302665742176, 3.0780221964605343], 0.009715909877514362)
    #
    # B...
    # :: hit rate = 0
    # :: median = 0.010470020726820461
    # >> ([1.962972623335176, -0.8064035446183908, 2.3149739813680483], 0.009719840927134737)
    # >> ([-0.656962707978586, -3.043915292699481, -0.3675224149334042], 0.009723983615166132)
    # >> ([0.8644346544431016, -2.9445423815452543, -0.6969216808899361], 0.009786195136224252)
    # >> ([-2.272964607698732, 2.116991098876518, 0.5691769452938829], 0.010083689460061718)
    # >> ([-0.5718949183916635, -1.8113860184602828, -2.5244486448519865], 0.010140071521760496)
    # >> ([2.5022757064226653, 0.9031538501519378, 1.601720620022121], 0.010799969931880427)
    # >> ([-1.282764092809721, -2.2985230915656487, -1.8121489582772057], 0.01292543082173181)
    # >> ([-1.340938253850105, -2.2198449229454624, 1.6354807069209585], 0.014860885643566302)
    # >> ([2.2134076182401685, -4.794862172537883, 3.4256236198537877], 0.037513401800366886)
    # >> ([-0.6019356736830161, 0.5813839823646489, -6.245973704325536], 0.0377862832658209)
    #
    # -- P_MUTATION = 0.6
    # FP...
    # :: hit rate = 1
    # :: median = 0.009715909877514362
    # >> ([6.824715904071238e-10, -3.770959131999267e-11, 1.3224625649960665e-09], 0.0)
    # >> ([-2.164968626210965, 2.1235687236657794, 0.8083648433391191], 0.009715909877514362)
    # >> ([1.4230414161736153, 2.705395987003288, 0.7112472401564065], 0.009715909877514362)
    # >> ([-0.6681938064908363, 2.729715640872038, 1.3972317369109972], 0.009715909877514362)
    # >> ([-0.26720449693173826, -2.8757633999137844, -1.2282807453452003], 0.009715909877514362)
    # >> ([-2.3515885851738454, -1.360908354570636, -1.5710017656043227], 0.009715909877514362)
    # >> ([2.6909234119939502, 0.15170063917908364, -1.6081060531702929], 0.009715909877514362)
    # >> ([1.8690508336487872, 1.3398935970440977, 2.1357483702543587], 0.009715909877514362)
    # >> ([-2.3129238039276387, -1.1087802619939766, -1.8086118377663971], 0.009715909877514362)
    # >> ([-1.042662673024592, 0.791628040390587, -2.8524141652739456], 0.009715909877514362)
    #
    # B...
    # :: hit rate = 0
    # :: median = 0.03737634852593452
    # >> ([-0.5310776381862823, -0.608516029603976, 3.0092492147680403], 0.010223128799928538)
    # >> ([-2.017570503983741, 1.9390353865792207, 1.3124710619311628], 0.011947232771016614)
    # >> ([1.042390366740399, 0.39398688983291663, 3.0326142466613035], 0.018064098310758614)
    # >> ([1.4764077550925023, -5.716493471380936, -2.137447422717777], 0.037227514299484954)
    # >> ([0.47018550404811776, 5.459811906724887, -3.039719123706405], 0.0373267718544017)
    # >> ([-3.7349480318775363, 4.406382754508378, -2.494407889560648], 0.03742592519746735)
    # >> ([3.455855110099371, 0.9974722850190503, -5.172517381914801], 0.037717236816518396)
    # >> ([-2.5954020478258357, -4.5649311852127, -3.3615843589708163], 0.03887166396774072)
    # >> ([-5.670430979934203, 0.6299260282163743, -2.4416696747158397], 0.041933514057849286)
    # >> ([-0.7574800288581969, 2.0500431299415283, -6.0542850753236195], 0.0605438778279091)
    #
    # -- P_MUTATION = 0.9
    # FP...
    # :: hit rate = 0
    # :: median = 0.009715909878191764
    # >> ([3.025707241965075, 0.0237076427296518, -0.833439038315697], 0.009715909877514417)
    # >> ([1.4554001002603363, 1.2025818537075508, -2.5071286944503446], 0.009715909877514972)
    # >> ([0.21509571527569418, -2.5979077594260884, 1.7477688390172552], 0.00971590987751525)
    # >> ([1.1439427693011457, 2.6815553088926367, 1.1622135432403675], 0.009715909877699047)
    # >> ([2.3895304239783397, 1.9357462526461973, 0.6269878400771358], 0.009715909877917595)
    # >> ([-1.1038685784920115, -1.4264952680662208, 2.5683975952782454], 0.009715909878465934)
    # >> ([-1.664105907515057, 2.58072693678934, 0.6485919555593431], 0.009715909883466434)
    # >> ([-2.935018551759779, -0.6447318879526248, -0.9055887745803506], 0.009715909884850271)
    # >> ([1.5154053578702005, -0.9480670022856761, 2.5797571582152283], 0.009715912833377582)
    # >> ([3.0262137990158293, -0.17526903508157884, -0.8130478800059342], 0.009715912945550909)
    #
    # B...
    # :: hit rate = 0
    # :: median = 0.039035422751738935
    # >> ([1.133943144771166, 1.019883642141174, 2.795339963598231], 0.011776097647351103)
    # >> ([-1.3633496109722216, 0.16071804080869612, -2.8935207812885224], 0.013750376800628017)
    # >> ([-1.3150936675518352, -1.9746551392818148, 2.2038708705286325], 0.019430426343984675)
    # >> ([5.436637609785841, -3.1351819682988946, -0.14779574765956482], 0.03722426414311225)
    # >> ([2.6295912883716994, -3.4503714801652308, 4.582812587171837], 0.03825176621194276)
    # >> ([-0.22332678953494423, -1.1008029464735714, -6.229665865738802], 0.03981907929153511)
    # >> ([2.7356399229240083, -4.365803892995785, 3.697897767018212], 0.04108743288684552)
    # >> ([-4.608895592162895, 0.36928671326003837, -4.347779439820975], 0.04170285057900924)
    # >> ([-1.4069325480139483, 2.4284612791353624, 5.497958897571046], 0.04727704296282209)
    # >> ([-5.931928602184584, -0.5059006242278201, 1.3076073205982794], 0.067589422124193)
    #
    # HIT RATE / MEDIAN - comparisons
    # ** ** *f6 ** ** *
    #
    # POP_SIZE = 30 - -----
    # --- P_MUTATION = 0.1
    # FP:
    # (0, 0.02346999249250109)
    # B:
    # (0, 0.009715909947296097)
    # --- P_MUTATION = 0.3
    # FP:
    # (0, 0.009715909877514362)
    # B:
    # (0, 0.011591551175977516)
    # --- P_MUTATION = 0.6
    # FP:
    # (0, 0.009715909877514362)
    # B:
    # (0, 0.038481913326057715)
    # --- P_MUTATION = 0.9
    # FP:
    # (0, 0.009715909877514417)
    # B:
    # (0, 0.01219755298020539)
    #
    # POP_SIZE = 50 - -----
    # --- P_MUTATION = 0.1
    # FP:
    # (0, 0.03722407510748782)
    # B:
    # (0, 0.009715910085578316)
    # --- P_MUTATION = 0.3
    # FP:
    # (0, 0.009715909877514362)
    # B:
    # (0, 0.010472343526134104)
    # --- P_MUTATION = 0.6
    # FP:
    # (0, 0.009715909877514362)
    # B:
    # (0, 0.044814214987516976)
    # --- P_MUTATION = 0.9
    # FP:
    # (0, 0.009715909877514362)
    # B:
    # (0, 0.037371484403063954)
    #
    # POP_SIZE = 100 - -----
    # --- P_MUTATION = 0.1
    # FP:
    # (0, 0.009715909877514362)
    # B:
    # (0, 0.00971591028760338)
    # --- P_MUTATION = 0.3
    # FP:
    # (1, 0.009715909877514362)
    # B:
    # (0, 0.01515809998931969)
    # --- P_MUTATION = 0.6
    # FP:
    # (0, 0.009715909877514362)
    # B:
    # (0, 0.03147139654201725)
    # --- P_MUTATION = 0.9
    # FP:
    # (0, 0.00971590987751439)
    # B:
    # (0, 0.017391468556801903)
    #
    # POP_SIZE = 200 - -----
    # --- P_MUTATION = 0.1
    # FP:
    # (0, 0.009715909877514362)
    # B:
    # (0, 0.009715910950772039)
    # --- P_MUTATION = 0.3
    # FP:
    # (0, 0.009715909877514362)
    # B:
    # (0, 0.010470020726820461)
    # --- P_MUTATION = 0.6
    # FP:
    # (1, 0.009715909877514362)
    # B:
    # (0, 0.03737634852593452)
    # --- P_MUTATION = 0.9
    # FP:
    # (0, 0.009715909878191764)
    # B:
    # (0, 0.039035422751738935)



def zad5_load_params():
    fp_solve_yn, b_solve_yn = False, False

    with open("zad5_params.txt", "r") as params_file:
        lines = params_file.readlines()
        lines = [l.split("=") for l in lines]
        for l in lines:
            if l[0].strip() == "MAX_NUM_EVAL":
                MAX_NUM_EVAL = int(float(l[1].strip()))
            elif l[0].strip() == "F_LIST":
                f_num_list = l[1].strip().split()
            elif l[0].strip() == "N_VAR":
                N_VAR = int(l[1].strip())
            elif l[0].strip() == "FP":
                fp_solve_yn = int(l[1]) == 1
            elif l[0].strip() == "B":
                b_solve_yn = int(l[1]) == 1
            elif l[0].strip() == "K_LIST":
                K_test_tournament_range = l[1].strip().split()
                K_test_tournament_range = [int(el) for el in K_test_tournament_range]
            elif l[0].strip() == "POP_SIZE_FP":
                POP_SIZE_FP = int(l[1].strip())
            elif l[0].strip() == "POP_SIZE_B":
                POP_SIZE_B = int(l[1].strip())
            elif l[0].strip() == "EXPLICIT_CONSTRAINT":
                EXPLICIT_CONSTRAINT = l[1].strip().split()
                EXPLICIT_CONSTRAINT = tuple([float(el) for el in EXPLICIT_CONSTRAINT])
            elif l[0].strip() == "P_MUTATION_FP":
                P_MUTATION_FP = float(l[1].strip())
            elif l[0].strip() == "P_MUTATION_B":
                P_MUTATION_B = float(l[1].strip())
            else: # N_DECIMAL
                N_DECIMAL = int(l[1].strip())

        return MAX_NUM_EVAL, f_num_list, N_VAR, fp_solve_yn, b_solve_yn, K_test_tournament_range, POP_SIZE_FP, POP_SIZE_B, EXPLICIT_CONSTRAINT, P_MUTATION_FP, P_MUTATION_B, N_DECIMAL

def zad5():
    print()
    print()
    print("************************* ZAD 5 *************************")

    MAX_NUM_EVAL, f_num_list, N_VAR, fp_solve_yn, b_solve_yn, K_test_tournament_range, POP_SIZE_FP, POP_SIZE_B, EXPLICIT_CONSTRAINT, P_MUTATION_FP, P_MUTATION_B, N_DECIMAL = zad5_load_params()


    f_goal_list = []
    if "f1" in f_num_list:
        f_goal_list.append(f1_goal)
    if "f3" in f_num_list:
        f_goal_list.append(f3_goal)
    if "f6" in f_num_list:
        f_goal_list.append(f6_goal)
    if "f7" in f_num_list:
        f_goal_list.append(f7_goal)

    ga_type_list = []
    if fp_solve_yn:
        ga_type_list.append("FP")
    if b_solve_yn:
        ga_type_list.append("B")

    f_goal_dict = dict(zip(f_goal_list, f_num_list))


    f_sol_list = []

    for fx_goal in f_goal_list:
        print()
        print("***** " + f_goal_dict[fx_goal] + " *****")
        f_sol_K_size_list = []
        for K_tournament_size in K_test_tournament_range:
            print("K = ", K_tournament_size)
            fx_list = []

            if fp_solve_yn:
                print("FP...")
                fp_list = []
                for i in range(GA_REPEAT_TIMES):
                    if print_yn:
                        print()
                        print("%5d run:" % (i + 1))

                    sol = runKTournamentEliminationSelection(sol_form="FP", max_f_eval=MAX_NUM_EVAL, K=K_tournament_size, f_goal=fx_goal, pop_size=POP_SIZE_FP,
                                                             explicit_constraint=EXPLICIT_CONSTRAINT, n_var=N_VAR,
                                                             pm=P_MUTATION_FP)
                    print(i, "SOL =", sol)
                    fx_goal.reset_called_times()
                    fp_list.append(sol)

                fp_list.sort(key=itemgetter(1))
                hit_rate, median = get_hit_rate_and_median(fp_list)
                print(":: hit rate =", hit_rate)
                print(":: median =", median)
                fx_list.append((hit_rate, median))
                for sol in fp_list:
                    print(">>",sol)
                print()

            if b_solve_yn:
                print("B...")
                b_list = []
                for i in range(GA_REPEAT_TIMES):
                    if print_yn:
                        print()
                        print("%5d run:" % (i + 1))

                    sol = runKTournamentEliminationSelection(sol_form="B", max_f_eval=MAX_NUM_EVAL, K=K_tournament_size, f_goal=fx_goal, pop_size=POP_SIZE_B,
                                                             explicit_constraint=EXPLICIT_CONSTRAINT, n_var=N_VAR,
                                                             pm=P_MUTATION_B, num_decimal=N_DECIMAL)
                    print(i, "SOL =", sol)
                    fx_goal.reset_called_times()
                    b_list.append(sol)
                b_list.sort(key=itemgetter(1))
                hit_rate, median = get_hit_rate_and_median(b_list)
                print(":: hit rate =", hit_rate)
                print(":: median =", median)
                fx_list.append((hit_rate, median))
                for sol in b_list:
                    print(">>",sol)
                print()

            f_sol_K_size_list.append(fx_list)
        f_sol_list.append(f_sol_K_size_list)

    print("")
    print("HIT RATE / MEDIAN - comparisons")
    for i in range(len(f_num_list)):
        print("***** " + f_num_list[i] + " *****")
        f_K_vals_list = f_sol_list[i]
        for k in range(len(f_K_vals_list)):
            print("K =", K_test_tournament_range[k], "------")
            fx_vals = f_K_vals_list[k]
            for j in range(len(ga_type_list)):
                print("  "+ga_type_list[j]+":")
                print(fx_vals[j])
        print()

    # ** ** *f6 ** ** *
    # K = 3
    # FP...
    # :: hit rate = 0
    # :: median = 0.009715909877514362
    # >> ([0.7243715620524785, -2.9911005137617312, 0.6153783540188581], 0.009715909877514362)
    # >> ([2.1588973974592025, 0.4309556024562195, 2.2368563381018203], 0.009715909877514362)
    # >> ([0.7887019805074543, -2.0840852063308843, 2.2101187758219787], 0.009715909877514362)
    # >> ([-0.18691605944831227, 1.7325963930486192, -2.61022203849868], 0.009715909877514362)
    # >> ([1.833730168305739, 0.5432363494499403, -2.488456329099009], 0.009715909877514362)
    # >> ([0.5106685283548045, -2.374469830063388, -1.9877619743686759], 0.009715909877514362)
    # >> ([3.0534286419567778, 0.19710347021881044, -0.6984344793129224], 0.009715909877514362)
    # >> ([-0.8474325384855456, -2.5499952310211946, 1.6215638722782992], 0.009715909877514362)
    # >> ([2.1961121834025983, -2.0697234217558664, -0.8622199321645134], 0.009715909877514362)
    # >> ([-0.6384266409843073, -0.47442721335916793, 3.0360199635893457], 0.009715909877514362)
    #
    # B...
    # :: hit rate = 1
    # :: median = 0.009715909888548258
    # >> ([0.0004053117777402804, -7.152560783652007e-05, 0.00011920934639420011], 1.8378801119878219e-07)
    # >> ([2.8997673510395714, 0.17817028912081412, -1.1873012482172243], 0.009715909878871276)
    # >> ([-2.1460304956581595, -1.1608844570562695, -1.9740829344191226], 0.009715909883945884)
    # >> ([2.0233402363492132, 2.3571264062530517, -0.4473449932789748], 0.009715909886343244)
    # >> ([2.197099779653442, 1.4652497602700052, 1.6958483199350027], 0.009715909886366891)
    # >> ([1.2499099969434795, -2.4765741713400686, -1.4677769984135622], 0.009715909890729624)
    # >> ([2.1549473547684457, -0.9240870113787736, -2.0862350875068074], 0.009715909892801355)
    # >> ([3.079630412879183, 0.6012204176046438, -0.06673339211148743], 0.009715909926095112)
    # >> ([0.015854843070428615, -1.425147736142982, 2.796198270892269], 0.009715909981992121)
    # >> ([-0.17592915340859605, -3.103519965896588, 0.43289682049599776], 0.009715910126276373)
    #
    # K = 4
    # FP...
    # :: hit rate = 0
    # :: median = 0.009715909877514362
    # >> ([-0.43635896792702433, -0.24416451892299018, -3.0983966026021843], 0.009715909877514362)
    # >> ([2.68591768179515, 0.8223439511751285, -1.3998870045476532], 0.009715909877514362)
    # >> ([-1.9547841872893714, -2.1283883902824674, -1.2242828889866602], 0.009715909877514362)
    # >> ([-0.779689357106209, 1.961315411008216, 2.3228029037820432], 0.009715909877514362)
    # >> ([-2.6302019702473483, 1.6992295287626926, -0.21152677063227188], 0.009715909877514362)
    # >> ([2.324986846341322, -2.0998953720147506, 0.18698280890959723], 0.009715909877514362)
    # >> ([-1.3606403224059214, 1.0629516607309881, 2.620854569298714], 0.009715909877514362)
    # >> ([-0.6659436282266437, -2.8172427406854372, 1.2123322164256052], 0.009715909877514362)
    # >> ([1.0208371290119764, -1.1233116307215243, 2.7470255749969468], 0.009715909877514362)
    # >> ([-2.2370386621452765, 1.1278985921018678, 1.8903940740910044], 0.009715909877514362)
    #
    # B...
    # :: hit rate = 0
    # :: median = 0.009715909878690698
    # >> ([-2.505756619337376, -1.889778084649123, 0.0029802336598550028], 0.009715909877524909)
    # >> ([0.9955649354767431, 2.9753937603920733, 0.07727149833274183], 0.009715909877569151)
    # >> ([-3.107525399935433, 0.43613931471792, 0.05619528589024014], 0.009715909877676177)
    # >> ([0.9268049844765613, 1.9047984622947993, -2.315784604923536], 0.009715909877908657)
    # >> ([-0.03507138970918078, -3.0883565370352386, -0.5575897968243595], 0.009715909878017237)
    # >> ([2.0283947126363415, -1.4749295591972142, 1.8868693765971116], 0.00971590987936416)
    # >> ([1.5980966558917373, 2.6881469193205447, 0.26466859086445993], 0.009715909879654872)
    # >> ([0.6765607245257996, -0.5495789287466692, -3.0150189471335125], 0.009715909891494623)
    # >> ([-1.5187032311931716, -1.3499027966989487, 2.3919355354001723], 0.009715909906832243)
    # >> ([2.4986517422922816, -1.6624220192060548, -0.9181742277976213], 0.009715910335345745)
    #
    # K = 5
    # FP...
    # :: hit rate = 0
    # :: median = 0.009715909877514362
    # >> ([-2.8754983600823665, 1.0851302410130235, -0.635679570129919], 0.009715909877514362)
    # >> ([0.6690534224005473, -2.6303294393159447, -1.576014446978623], 0.009715909877514362)
    # >> ([-2.6564097305843446, -1.073747877566159, 1.2808746241519737], 0.009715909877514362)
    # >> ([3.11343677286203, -0.20060094120771732, -0.34111244975802785], 0.009715909877514362)
    # >> ([3.075267234648537, 0.20514359208085123, -0.5922284354409817], 0.009715909877514362)
    # >> ([-1.2015946821015644, -2.649433398535702, 1.1776076862670142], 0.009715909877514362)
    # >> ([-1.3109073281358374, -2.4735328624326476, -1.4188883406569734], 0.009715909877514362)
    # >> ([-0.784960965299917, 3.023245262060537, 0.3064495623209401], 0.009715909877514362)
    # >> ([1.8997472139066813, 2.488315237455337, -0.22211432082025864], 0.009715909877514362)
    # >> ([-0.07214216591583845, 3.0194944263392114, -0.852957146750278], 0.009715909877514362)
    #
    # B...
    # :: hit rate = 1
    # :: median = 0.009715909879057405
    # >> ([-7.152560783652007e-05, 2.3841869278840022e-05, 2.3841869278840022e-05], 6.259034790456752e-09)
    # >> ([-2.9297604225923664, -0.9573702608920343, -0.5916359861545502], 0.009715909877514861)
    # >> ([2.9911293941161077, -0.9443049165272299, 0.10731225362408026], 0.009715909877959839)
    # >> ([-2.0785103218604633, 0.023674976193888142, -2.3514520413646878], 0.009715909878238227)
    # >> ([-0.2783538238305212, -0.09591584010879473, 3.1246438620776473], 0.009715909878739049)
    # >> ([3.027941240282651, 0.7958654383971364, 0.21965514166599576], 0.009715909879375761)
    # >> ([-2.3401986790650753, -2.0899544191143136, -0.07522109757475448], 0.00971590988029769)
    # >> ([0.7786039250392562, 0.16567714961869484, 3.0358567408832258], 0.0097159098812018)
    # >> ([1.2521034489171328, 1.1546378873052134, -2.6361239605541016], 0.00971590988257881)
    # >> ([2.5504362823659292, 1.3015038020628964, 1.2851005959990474], 0.00971590990621779)
    #
    # K = 6
    # FP...
    # :: hit rate = 0
    # :: median = 0.009715909877514362
    # >> ([-1.018785196873288, -0.6695425548074144, -2.892036729828974], 0.009715909877514362)
    # >> ([-1.375251038110338, -1.8121168676768655, 2.1621757574467124], 0.009715909877514362)
    # >> ([0.6509319510151211, -1.1415097422453717, 2.8501456226590443], 0.009715909877514362)
    # >> ([-0.986123884345756, 1.2986236023776492, 2.681645654470382], 0.009715909877514362)
    # >> ([3.1010622432688746, 0.0492021094820262, 0.48070686008499397], 0.009715909877514362)
    # >> ([-0.4119982953190577, -3.0088268299878935, -0.7920261879921814], 0.009715909877514362)
    # >> ([0.9225495129931552, 2.9317724379491272, 0.6353736924800486], 0.009715909877514362)
    # >> ([2.652138556525218, 1.6733624700102294, 0.12690981055857284], 0.009715909877514362)
    # >> ([-2.079089623197528, 1.143593638106562, 2.054182783485758], 0.009715909877514362)
    # >> ([0.010615303479266897, 2.8746630375376316, -1.2594787401973484], 0.009715909877514362)
    #
    # B...
    # :: hit rate = 0
    # :: median = 0.00971590987877971
    # >> ([-3.096367405112936, 0.45368693050714626, 0.23825179970350518], 0.009715909877514917)
    # >> ([1.1644607374480955, -0.038313883931110126, -2.9142155238225556], 0.009715909877605233)
    # >> ([-0.6160500602960894, 2.474142300673634, -1.8300780439749005], 0.009715909877824502)
    # >> ([2.7987731927743837, -1.4185673802220222, 0.0680208530525448], 0.00971590987805504)
    # >> ([-2.1767388232893126, 1.0305647995780944, -2.0124206602195045], 0.00971590987830967)
    # >> ([-2.6670230231394854, -1.5889890618272133, 0.46064875633656754], 0.009715909879249751)
    # >> ([2.58400563431055, 1.6222246276019234, 0.7357839278144453], 0.009715909880598728)
    # >> ([2.60641699143266, -0.6468537554043507, -1.6242750283599037], 0.00971590988092863)
    # >> ([-1.1532550588870336, 2.493501898528052, 1.5174157702521143], 0.009715909881465257)
    # >> ([-2.9250874162137137, -0.897479485263581, -0.6989243979093587], 0.009715909882507867)
    #
    # K = 7
    # FP...
    # :: hit rate = 0
    # :: median = 0.009715909877514362
    # >> ([-2.0414075277819332, 1.1463896925444224, 2.0901035715028904], 0.009715909877514362)
    # >> ([-1.1117400736227678, -2.367457837687765, -1.7347231379040564], 0.009715909877514362)
    # >> ([0.5172639048708927, 0.1231395267133838, 3.0931152080543356], 0.009715909877514362)
    # >> ([1.167256144608081, -2.3189464489786613, -1.7635439988494612], 0.009715909877514362)
    # >> ([-0.20469154529443206, -1.8167536173308743, 2.5509987532422755], 0.009715909877514362)
    # >> ([0.9849121808261306, -2.290146730662702, 1.9066365446144833], 0.009715909877514362)
    # >> ([-0.44071987890119035, 3.022754257416764, -0.7202844305879195], 0.009715909877514362)
    # >> ([2.3113104720463107, 2.1071835072700615, -0.2602086655845599], 0.009715909877514362)
    # >> ([-0.548337288115667, 0.5579951269059966, 3.0394168254813314], 0.009715909877514362)
    # >> ([3.348558219223029, -5.02624765620637, -1.7107189851535893], 0.03722407510748782)
    #
    # B...
    # :: hit rate = 1
    # :: median = 0.009715909882227591
    # >> ([2.3841869278840022e-05, -7.152560783652007e-05, 2.3841869278840022e-05], 6.259034790456752e-09)
    # >> ([-3.036858099392937, 0.7279637946909858, -0.3124953806378272], 0.009715909878086071)
    # >> ([1.4498002289773169, 2.662779170407859, 0.8110288672584929], 0.009715909878278584)
    # >> ([2.843023702155925, -1.310802131081644, -0.22161017494686774], 0.009715909880051998)
    # >> ([0.4694225642311949, -0.028967871173797732, -3.103043128511011], 0.009715909882147267)
    # >> ([-1.2199169253906845, -2.056671169601046, -2.0327339328450833], 0.009715909882307916)
    # >> ([2.932955233075731, -0.8946184609501202, -0.6689790100951214], 0.009715909882774376)
    # >> ([1.0502104998638657, 2.6201499081372788, -1.3718373164354887], 0.009715909889264462)
    # >> ([1.5313394219109568, -1.9050368809875877, -1.9687423557006625], 0.009715909893625863)
    # >> ([2.224088775677103, -0.7937196701620479, -2.0672569595608508], 0.009715909894561114)
    #
    # K = 8
    # FP...
    # :: hit rate = 0
    # :: median = 0.009715909877514362
    # >> ([-0.7590796267046583, -1.2939477349903803, 2.756734365196774], 0.009715909877514362)
    # >> ([0.396338149596449, 2.399423904166459, -1.9838770065429472], 0.009715909877514362)
    # >> ([1.4595240711034532, -2.4597553123866023, -1.2920836965159228], 0.009715909877514362)
    # >> ([3.1165723770141245, -0.3047567753204675, 0.2102068371377654], 0.009715909877514362)
    # >> ([-1.0998410185350052, 1.8620817304271744, -2.2744424243242443], 0.009715909877514362)
    # >> ([-0.5076620609692932, -1.0251516022844547, 2.9225725634631985], 0.009715909877514362)
    # >> ([1.2088209067566664, 0.7654686399863236, 2.793366563211994], 0.009715909877514362)
    # >> ([-0.9330517019721735, 1.039106019103186, -2.810651201671885], 0.009715909877514362)
    # >> ([0.5888823673916618, 2.2314325265411963, 2.126972829507837], 0.009715909877514362)
    # >> ([-1.079774264673834, 1.9105346864480917, 2.2436647950468127], 0.009715909877514362)
    #
    # B...
    # :: hit rate = 3
    # :: median = 0.009715909878179274
    # >> ([-7.152560783652007e-05, 2.3841869278840022e-05, 2.3841869278840022e-05], 6.259034790456752e-09)
    # >> ([-7.152560783652007e-05, 2.3841869278840022e-05, -7.152560783652007e-05], 1.0811060102700054e-08)
    # >> ([2.3841869278840022e-05, -7.152560783652007e-05, -7.152560783652007e-05], 1.0811060102700054e-08)
    # >> ([-3.058029679312554, 0.7056478050459845, -0.02458096722649117], 0.009715909877577755)
    # >> ([0.01060963182908381, -2.7457011917596787, -1.5202291108270174], 0.009715909877713702)
    # >> ([0.5770924458944577, -2.2129784645931565, -2.149368357357197], 0.009715909878644846)
    # >> ([2.525450003361705, 0.19943723651753942, -1.8526801360512408], 0.009715909879830564)
    # >> ([-2.3938905686810372, 0.6618741330500342, -1.9186744302150913], 0.009715909882408946)
    # >> ([0.5510571246419502, -0.9552721763954963, -2.9383434955327488], 0.009715909883571072)
    # >> ([-1.919628104986245, -1.9830474772679665, -1.494193789574524], 0.009715909890167074)
    #
    # HIT RATE / MEDIAN - comparisons
    # ** ** *f6 ** ** *
    # K = 3 - -----
    # FP:
    # (0, 0.009715909877514362)
    # B:
    # (1, 0.009715909888548258)
    # K = 4 - -----
    # FP:
    # (0, 0.009715909877514362)
    # B:
    # (0, 0.009715909878690698)
    # K = 5 - -----
    # FP:
    # (0, 0.009715909877514362)
    # B:
    # (1, 0.009715909879057405)
    # K = 6 - -----
    # FP:
    # (0, 0.009715909877514362)
    # B:
    # (0, 0.00971590987877971)
    # K = 7 - -----
    # FP:
    # (0, 0.009715909877514362)
    # B:
    # (1, 0.009715909882227591)
    # K = 8 - -----
    # FP:
    # (0, 0.009715909877514362)
    # B:
    # (3, 0.009715909878179274)

if __name__ == "__main__":
    with open("results.txt", "w") as results_file:
        zad1()
        zad2()
        zad3()
        zad4()
        zad5()